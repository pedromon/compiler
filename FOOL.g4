grammar FOOL;

@header{
 import node.*;
 import node.interfaces.*;
 import node.abstractnode.*;
 import node.tables.*;
 import type.*;
 import java.util.*;
 import node.object.*;
 import utils.*;
}

@lexer::members {
public int lexicalErrors=0;
}

@parser::members {
private SymbolTable symTable = new SymbolTable();
private ClassTable classTable = new ClassTable();
}
/*------------------------------------------------------------------
 * PARSER RULES
 *------------------------------------------------------------------*/

prog returns [Node ast]:
        { symTable.enterNestingLevel(); }
        ( LET
            {
                int offset = -2;
                List<ClassNode> classList = new ArrayList<>();
                List<DeclarationNode> decList = new ArrayList<>();
            }
        ( cllist {classList = $cllist.classList;} (declist {decList = $declist.decList;} )? | declist {decList = $declist.decList;}) IN exp {$ast = new ProgLetInNode(classList, decList, $exp.ast); }
        | exp { $ast=new ProgNode($exp.ast); }
        ) SEMIC
        { symTable.exitNestingLevel(); }
      ;

cllist returns [List<ClassNode> classList] : { int offset = -2; $classList = new ArrayList<>(); }
            ( CLASS baseClassName = ID
            {
                Optional<String> superclassName = Optional.empty();
                if(symTable.findClassDeclaration($baseClassName.text).isPresent()) {
                    throw new RuntimeException("Multiple class declaration " + $baseClassName.text + " at line " + $baseClassName.line);
                }
            }
            (EXTENDS superClassName = ID {
                superclassName = Optional.of($superClassName.text);
            })?
            {
                Set<String> thisMethodAndField = new HashSet<>();
                InheritanceMap.getInstance().addInheritanceRel($baseClassName.text, superclassName);
                // se non si eredita creo un ClassType con le liste vuote
                ClassType classType = new ClassType();
                Optional<STEntry> superClassEntry = Optional.empty(); //non è detto che se superclassname non è empty anche questa è piena; potrei aver esteso da una classe non esistente
                if(superclassName.isPresent()){
                    superClassEntry = symTable.findClassDeclaration($superClassName.text);
                    if(!superClassEntry.isPresent()) {
                        throw new RuntimeException("Extend to non existing class " + $superClassName.text + " at line " + $superClassName.line);
                    } else {
                        // recupero il class type della classe padre (super)
                        // sono certo che il downcast è corretto, perchè sono sicuro che la STEntry è di una classe e che quindi
                        // il tipo è ClassType, perchè possiamo avere solo dichiarazioni di classi prima di variabili o funzioni
                        ClassType superClassType = (ClassType) superClassEntry.get().getType(); // prendo la ClassType della classe padre, cioè da cui estendo.
                        classType = new ClassType(superClassType); // costruisco la mia ClassType a partire dalla ClassType della super classe.
                    }
                }

                ClassNode classNode = new ClassNode($baseClassName.text, classType);
                if(superClassEntry.isPresent() && superclassName.isPresent()) {
                    classNode.addSuperClass(superclassName.get(), superClassEntry.get());
                }
                $classList.add(classNode);

                 // aggiungo dichiarazione della classe in symbol table, non controllo se ci sono duplicati, perchè l'ho
                 // già fatto in precendenza con class table, quindi se c'era una classe con stesso nome, avrei già lanciato l'eccezione.
                 STEntry classEntry = new STEntry(classType, symTable.getCurrentNestingLevel(), offset--);
                 symTable.addEntry($baseClassName.text, classEntry);
                 VirtualTable virtualTable = classTable.initVirtualTable($baseClassName.text, Optional.ofNullable($superClassName.text));//crea una virtual table vuota se non eredito, altrimenti ne creo una a partire dalla virtual della classe da cui eredito

                 // entro in nesting level 1
                 symTable.enterNestingLevel();
                 symTable.setReferenceToVirtualTable(virtualTable);
            }
            LPAR (fieldName = ID COLON type
                    {
                        if(thisMethodAndField.contains($fieldName.text)){
                            throw new RuntimeException("multiple declaration for field: "+$fieldName.text +" at line "+$fieldName.line);
                        }else{
                            thisMethodAndField.add($fieldName.text);
                        }
                        // Offset dei campi viene inizializzato a -1 se non si eredita, altrimenti lo setto in base alla
                        // al numero di campi della classe da cui estendo, ovvero "-lunghezza-1"
                        int offsetField = -1;
                        if(superClassEntry.isPresent()) {
                            ClassType superClassType = ((ClassType) superClassEntry.get().getType());
                            offsetField = -(superClassType.getFieldsCount()) - 1;
                        }

                        // aggiungo nodo del campo al nodo della classe.
                        FieldNode fieldNode = new FieldNode($fieldName.text, $type.mType);
                        classNode.addFieldNode(fieldNode);
                        STEntry fieldEntry = new STEntry($type.mType, symTable.getCurrentNestingLevel(), offsetField--);
                        if(!virtualTable.addEntry($fieldName.text, fieldEntry)){
                            throw new RuntimeException("id already declared "+$fieldName.text+ " at line "+$fieldName.line);
                        }
                        fieldNode.setOffset(fieldEntry.getOffset());//lo recuperiamo dalla fieldENtry perchè la virtual potrebbe aver cambiato
                        // per mantenere coerenza con la virtual table in caso di override, il vecchio tipo del campo
                        // viene sostituito dal nuovo tipo del campo dichiarato per l'override, infatti viene rimpiazzato
                        // in posizione -offset-1. Dove l'offset è quello inserito nella STEntry del nuovo campo.
                        classType.addFieldType(-fieldEntry.getOffset() - 1, fieldEntry.getType());
                    }
                (COMMA fieldName = ID COLON type
                {
                    if(thisMethodAndField.contains($fieldName.text)){
                        throw new RuntimeException("multiple declaration for field: "+$fieldName.text +" at line "+$fieldName.line);
                    }else{
                        thisMethodAndField.add($fieldName.text);
                    }
                    fieldNode = new FieldNode($fieldName.text, $type.mType);
                    classNode.addFieldNode(fieldNode);
                    fieldEntry = new STEntry($type.mType, symTable.getCurrentNestingLevel(), offsetField--);
                    if(!virtualTable.addEntry($fieldName.text, fieldEntry)){
                        throw new RuntimeException("id already declared "+$fieldName.text+ " at line "+$fieldName.line);
                    }
                    fieldNode.setOffset(fieldEntry.getOffset());//lo recuperiamo dalla fieldENtry perchèla virtual potrebbe aver cambiato
                    classType.addFieldType(-fieldEntry.getOffset() - 1, fieldEntry.getType());
                })* )?
            RPAR
              CLPAR
                {
                    // Offset dei metodi viene inizializzato a 0 se non si eredita, altrimenti lo setto in base alla
                    // al numero di metodi della classe da cui estendo, ovvero "lunghezza"
                    int offsetMethod = 0;
                    if(superClassEntry.isPresent()) {
                        ClassType superClassType = ((ClassType) superClassEntry.get().getType());
                        offsetMethod = superClassType.getMethodsCount();
                    }
                }
                 ( FUN methodName = ID COLON retType = type
                    {
                        if(thisMethodAndField.contains($methodName.text)){
                            throw new RuntimeException("multiple declaration of field or method: "+$methodName.text +" at line "+$methodName.line);
                        }else{
                            thisMethodAndField.add($methodName.text);
                        }

                        MethodNode methodNode = new MethodNode($methodName.text, $retType.mType);
                        classNode.addMethodNode(methodNode);
                        STEntry methodEntry = STEntry.makeMethodSTEntry(null, symTable.getCurrentNestingLevel(), offsetMethod++);
                        // aggiungo a virtual table il metodo
                        if(!virtualTable.addEntry($methodName.text, methodEntry)){
                            throw new RuntimeException("id already declared "+ $methodName.text+ " at line "+$methodName.line);
                        }
                        methodNode.setOffset(methodEntry.getOffset());
                        // entro nesting level
                        symTable.enterNestingLevel();
                    }
                    LPAR
                        (paramName = ID COLON parType = hotype
                            {
                                int offsetParameter = 1;
                                // higher order
                                if($parType.mType instanceof ArrowType){
                                    offsetParameter++;
                                }
                                methodNode.addParam(new ParNode($paramName.text, $parType.mType));
                                STEntry paramEntry = new STEntry($parType.mType, symTable.getCurrentNestingLevel(), offsetParameter++);

                                if (!symTable.addEntry($paramName.text, paramEntry)){ // controllo che non ci siano parametri con stesso nome
                                    throw new RuntimeException("parameter: "+$paramName.text+ " at line "+$paramName.line+ " already declared.");
                                }
                            }
                                (COMMA paramName = ID COLON parType = hotype
                                    {
                                        methodNode.addParam(new ParNode($paramName.text, $parType.mType));
                                        if($parType.mType instanceof ArrowType){ // higher order
                                            offsetParameter++;
                                        }
                                        paramEntry = new STEntry($parType.mType, symTable.getCurrentNestingLevel(), offsetParameter++);

                                        if (!symTable.addEntry($paramName.text, paramEntry)){ // controllo che non ci siano parametri con stesso nome
                                            throw new RuntimeException("parameter: "+$paramName.text+ " at line "+$paramName.line+ " already declared.");
                                        }
                                    }
                                )*
                        )?
                    RPAR
                    {
                        // possiamo costruire l'arrow type, che andiamo ad aggiungere a ClassType e alla STEntry che si trova nella virtual table.
                        methodEntry.addType(methodNode.getSymType());
                        // per mantenere coerenza con la virtual table in caso di override, il vecchio arrow type del metodo
                        // viene sostituito dal nuovo arrow type del metodo dichiarato per l'override, infatti viene rimpiazzato
                        // in posizione offset. Dove l'offset è quello inserito nella STEntry del nuovo metodo.
                        classType.addMethodType(methodEntry.getOffset(), methodNode.getSymType());
                    }
                        (LET
                            {  int offsetDec = -2;
                               List<DeclarationNode> decList = new ArrayList<>();
                            }
                            (VAR varName = ID COLON type ASS exp SEMIC
                                {
                                    decList.add(new VarNode($varName.text, $type.mType, $exp.ast));
                                    STEntry entry = new STEntry($type.mType, symTable.getCurrentNestingLevel(), offsetDec--);
                                    if (!symTable.addEntry($varName.text, entry)){
                                        throw new RuntimeException("variable: "+$varName.text+ " at line "+$varName.line+ " already declared.");
                                    };
                                }
                            )+
                            IN { methodNode.addDeclarationList(decList); }
                        )? exp
                            {
                                methodNode.addBody($exp.ast);
                                symTable.exitNestingLevel();
                            }
                    SEMIC
                 )*
              CRPAR
            { symTable.exitNestingLevel(); /* esco dal livello della dichiarazione della classe.*/ }
            )+
        ;

declist returns [List<DeclarationNode> decList]:
            {
                int offset = (symTable.getCurrentNestingLevel() == 0) ? -classTable.getNumberOfClassDeclaration()-2 : -2;
                $decList = new ArrayList<>();
            }
                (
            ( VAR nomeVar = ID COLON varType = hotype ASS exp {
                //aggiungo il nodo alla lisat delle dichiarazioni (la declist è un insieme di nodi "dichiarazione")
                $decList.add(new VarNode($nomeVar.text, $varType.mType, $exp.ast));
                STEntry entry= new STEntry($varType.mType, symTable.getCurrentNestingLevel(),  offset--);
                if($varType.mType instanceof ArrowType){
                    offset--;
                }
                if (!symTable.addEntry($nomeVar.text, entry)){
                    throw new RuntimeException("variable: "+$nomeVar.text+ " at line "+$nomeVar.line+ " already declared.");
                };
            }
            | FUN nomeFun = ID COLON retType = type {
                FunNode fun = new FunNode($nomeFun.text, $retType.mType);
                $decList.add(fun);
                //La costruzione della entry della funzione avviene in diversi passi; prima si passa il nesting level ma ancora prima di leggere i parametri
                //non possiamo sapere il suo tipo (tipo freccia); per questo passiamo null
                STEntry funEntry = STEntry.makeFunctionSTEntry(null, symTable.getCurrentNestingLevel(),  offset--);
                offset--; // le funzioni sono di tipo funzionale quindi occupano sempre offset doppio.
                if(!symTable.addEntry($nomeFun.text, funEntry)) {
                    throw new RuntimeException("Function : "+$nomeFun.text+ " at line "+$nomeFun.line+ " already declared.");
                }
                // entro dentro un nuovo scope
                symTable.enterNestingLevel();
            }
            LPAR (
            nomePar = ID COLON parType = hotype {
                int offsetParameter = 1;
                if($parType.mType instanceof ArrowType){
                    offsetParameter++;
                }

                ParNode parNode = new ParNode($nomePar.text, $parType.mType);
                fun.addParam(parNode);
                STEntry stEntry = new STEntry($parType.mType, symTable.getCurrentNestingLevel(), offsetParameter++);

                if (!symTable.addEntry($nomePar.text, stEntry)){
                    throw new RuntimeException("parameter: "+$nomePar.text+ " at line "+$nomePar.line+ " already declared.");
                };
            }

            (COMMA nomePar = ID COLON parType = hotype{
                parNode = new ParNode($nomePar.text, $parType.mType);
                fun.addParam(parNode);
                if($parType.mType instanceof ArrowType){
                    offsetParameter++;
                }
                stEntry = new STEntry($parType.mType, symTable.getCurrentNestingLevel(),  offsetParameter++);

                if (!symTable.addEntry($nomePar.text, stEntry)){
                    throw new RuntimeException("parameter: "+$nomePar.text+ " at line "+$nomePar.line+ " already declared.");
                };

            })*)? RPAR
            {
                funEntry.addType(fun.getSymType());//fun.getSymType() crea un arrowType in base ai parametry del FunNode e lo restituisce
                //dopo aver aggiunto le entry dei parametri setto il tipo della funzione nella entry
            }
            (LET declist IN {fun.addDeclarationList($declist.decList); })?
            exp
            {
                fun.addBody($exp.ast);
                symTable.exitNestingLevel();
            }
            ) SEMIC
          )+
        ;

exp	returns [Node ast]
            : term { $ast = $term.ast;}
            ( PLUS t2=term { $ast = new PlusNode($ast, $t2.ast);}
           | MINUS t2=term { $ast = new MinusNode($ast, $t2.ast);}
           | OR t2=term { $ast = new OrNode($ast, $t2.ast);}
           )*
    ;

term returns [Node ast]
                 : factor { $ast = $factor.ast;}
                 ( TIMES f2=factor { $ast = new MultNode($ast, $f2.ast);}
  	             | DIV  f2=factor { $ast = new DivNode($ast, $f2.ast);}
  	             | AND  f2=factor { $ast = new AndNode($ast, $f2.ast);}
  	             )*
  	    ;

factor returns [Node ast]
                :  value { $ast = $value.ast;}
                ( EQ v2=value { $ast = new EqualNode($ast, $v2.ast);}
	            | GE v2=value { $ast = new GreaterEqualNode($ast, $v2.ast);}
	            | LE v2=value { $ast = new LessEqualNode($ast, $v2.ast);}
	            )*
	    ;

value returns [Node ast]
        : val =
          INTEGER { $ast = new IntNode(Integer.parseInt($val.text)); }
	    | TRUE { $ast = new BoolNode(true); }
	    | FALSE { $ast = new BoolNode(false); }
	    | NULL { $ast = new EmptyNode(); }
	    | NEW id = ID
	        {
	            Optional<STEntry> classEntry = symTable.findClassDeclaration($id.text);
	            if(!classTable.existClass($id.text) || !classEntry.isPresent()) {
	                throw new RuntimeException("Instantiation of an undeclared class " + $id.text + " at line " + $id.line);
	            }
	        }
	        LPAR {List<Node> params = new ArrayList<>();} (exp {params.add($exp.ast);} (COMMA exp {params.add($exp.ast);})* )? RPAR
	        { $ast = new NewNode($id.text, classEntry.get(), params, symTable.getCurrentNestingLevel()); }

	    | IF cond=exp THEN CLPAR thenb=exp CRPAR ELSE CLPAR elseb=exp CRPAR { $ast = new IfNode($thenb.ast, $cond.ast, $elseb.ast); }
	    | NOT LPAR exp RPAR { $ast = new NotNode($exp.ast); }
	    | PRINT LPAR exp RPAR { $ast = new PrintNode($exp.ast); }
        | LPAR exp RPAR { $ast = $exp.ast; }
	    | id = ID
	        {
                Optional<STEntry> idEntry = symTable.findDeclaration($id.text);
                if(idEntry.isPresent()){
                    $ast= new IdNode($id.text, symTable.getCurrentNestingLevel(), idEntry.get());
                }else{
                    throw new RuntimeException("identificatore: "+$id.text+ " at line "+$id.line+ " never declared.");
                }
            } //riconosco un uso di una variabile
	    (

	        /* Chiamata a funzione */
	        LPAR { List<Node> parameters = new ArrayList();}
                (exp {parameters.add($exp.ast);} (COMMA exp{parameters.add($exp.ast);})* )? // parametri della chiamata a funzione (opzionali)
            RPAR { $ast= new CallNode($id.text, idEntry.get(), parameters,  symTable.getCurrentNestingLevel());}

            /* Chiamata a metodo */
            | DOT id2 = ID
                {
                    List<Node> parameters = new ArrayList();
                    Optional<STEntry> methodEntry = Optional.empty();
                    if(idEntry.get().getType() instanceof RefType) {
                        RefType refType = (RefType) idEntry.get().getType();
                        methodEntry = classTable.getSTEntry(refType.getClassName(), $id2.text);
                        if(!methodEntry.isPresent()) {
                            throw new RuntimeException("Method call to a non existing method " + $id2.text + " at line " + $id2.line);
                        }
                    } else {
                        throw new RuntimeException("Method call to a non object reference: " + $id.text + " at line " + $id.line);
                    }
                }
                LPAR (exp {parameters.add($exp.ast);} (COMMA exp {parameters.add($exp.ast);})* )? RPAR
                {
                    $ast = new ClassCallNode($id2.text, idEntry.get(), methodEntry.get(), parameters, symTable.getCurrentNestingLevel());
                }
        )?
        ;

hotype returns [Type mType] : type { $mType = $type.mType; }
        | arrow {$mType = $arrow.arrowType; }
        ;

type  returns [Type mType]
        : INT { $mType = IntType.TYPE; }
        | BOOL { $mType = BoolType.TYPE; }
 	    | className = ID
 	    {
            if(classTable.existClass($className.text)) {
                $mType = RefType.of($className.text);
            } else {
                throw new RuntimeException("Type " + $className.text + " at line " + $className.line + " unknown");
            }
 	    }
 	    ;

arrow  returns [ArrowType arrowType]: {List<Type> paramList = new ArrayList();}
            LPAR ( hotype {paramList.add($hotype.mType);}(COMMA hotype {paramList.add($hotype.mType);})* )? RPAR ARROW type
            {$arrowType = new ArrowType(paramList, $type.mType);};


/*------------------------------------------------------------------
 * LEXER RULES
 *------------------------------------------------------------------*/

PLUS  	: '+' ;
MINUS   : '-' ;
TIMES   : '*' ;
DIV 	: '/' ;
LPAR	: '(' ;
RPAR	: ')' ;
CLPAR	: '{' ;
CRPAR	: '}' ;
SEMIC 	: ';' ;
COLON   : ':' ;
COMMA	: ',' ;
DOT	    : '.' ;
OR	    : '||';
AND	    : '&&';
NOT	    : '!' ;
GE	    : '>=' ;
LE	    : '<=' ;
EQ	    : '==' ;
ASS	    : '=' ;
TRUE	: 'true' ;
FALSE	: 'false' ;
IF	    : 'if' ;
THEN	: 'then';
ELSE	: 'else' ;
PRINT	: 'print' ;
LET     : 'let' ;
IN      : 'in' ;
VAR     : 'var' ;
FUN	    : 'fun' ;
CLASS	: 'class' ;
EXTENDS : 'extends' ;
NEW 	: 'new' ;
NULL    : 'null' ;
INT	    : 'int' ;
BOOL	: 'bool' ;
ARROW   : '->' ;
INTEGER : '0' | ('-')?(('1'..'9')('0'..'9')*) ;

ID  	: ('a'..'z'|'A'..'Z')('a'..'z' | 'A'..'Z' | '0'..'9')* ;


WHITESP  : ( '\t' | ' ' | '\r' | '\n' )+    -> channel(HIDDEN) ;

COMMENT : '/*' (.)*? '*/' -> channel(HIDDEN) ;

ERR   	 : . { System.out.println("Invalid char: "+ getText()); lexicalErrors++; } -> channel(HIDDEN);
