package utils;

import node.interfaces.Node;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import svm.ExecuteVM;
import svm.SVMLexer;
import svm.SVMParser;
import test.FOOLLexer;
import test.FOOLParser;
import type.Type;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

public class TestUtils {

    public static FOOLParser getParser(String code) {
        CharStream chars = CharStreams.fromString(code);
        FOOLLexer lexer = new FOOLLexer(chars);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        return new FOOLParser(tokens);
    }

    public static String execute(String code) throws IOException {
        CharStream chars = CharStreams.fromString(code);
        FOOLLexer lexer = new FOOLLexer(chars);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        return execute(new FOOLParser(tokens), lexer);
    }

    public static String execute(FOOLParser parser, FOOLLexer lexer) throws IOException {
        Node ast = parser.prog().ast;

        if(lexer.lexicalErrors > 0 || parser.getNumberOfSyntaxErrors() > 0) {
            throw new RuntimeException("You had: "+lexer.lexicalErrors +" lexical errors and "+parser.getNumberOfSyntaxErrors()+" syntax errors.");
        }

        // Type check
        ast.typeCheck();

        // Code Gen
        String code = ast.codeGen();

        // Execution
        CharStream charsASM = CharStreams.fromString(code);
        SVMLexer lexerASM = new SVMLexer(charsASM);
        CommonTokenStream tokensASM = new CommonTokenStream(lexerASM);
        SVMParser parserASM = new SVMParser(tokensASM);
        parserASM.assembly();
        if (lexerASM.lexicalErrors > 0 || parserASM.getNumberOfSyntaxErrors() > 0) {
            throw new RuntimeException("You had: " + lexerASM.lexicalErrors + " lexical errors and "
                    + parserASM.getNumberOfSyntaxErrors() + " syntax errors.");
        }

        OutputStream out = redirectSysout();
        ExecuteVM vm = new ExecuteVM(parserASM.code);
        vm.cpu();

        out.flush();
        restoreSysout();

        return out.toString();
    }

    private static OutputStream redirectSysout() {
        // Create a stream to hold the output
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(baos);
        System.setOut(ps);
        return baos;
    }

    private static void restoreSysout() {
        System.setOut(OLD_SYS_OUT);
    }

    private static final PrintStream OLD_SYS_OUT = System.out;
}
