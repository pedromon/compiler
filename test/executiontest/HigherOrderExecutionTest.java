package executiontest;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import utils.TestUtils;

import java.io.IOException;

@RunWith(JUnit4.class)
public class HigherOrderExecutionTest extends AbstractExecutionTest {

    @Test
    public void higherOrder() {
        String code = "let\n" +
                "    fun filter:int (l:int, accept:(int)->bool)\n" +
                "              if (l == 0)\n" +
                "                then { 0 }\n" +
                "                else {if ( accept(l) )\n" +
                "                        then { 1 }\n" +
                "                        else { 0 }\n" +
                "                     };\n" +
                "     fun min2:bool(test:int)\n" +
                "             test == 2;           \n" +
                "in\n" +
                "    print(filter(2, min2));";

        assertNumeric(code, 1, (out) -> Integer.parseInt(out.trim()));
    }

    /*
    let
        var increment:int = 1;
        fun g:int(x:int)
            x + increment;

        fun f:int(y:(bool) -> int)
            y(true);
    in
        print(f(g));
     */
    @Test
    public void testOkParameterContravariance() throws IOException {
        String code = "let\n" +
                "    var increment:int = 1;\n" +
                "    fun g:int(x:int)\n" +
                "        x + increment;\n" +
                "\n" +
                "    fun f:int(y:(bool)->int)\n" +
                "        y(true);\n" +
                "in\n" +
                "    print(f(g));";

        int expectedOutput = 2;
        String output = TestUtils.execute(code);
        Assert.assertEquals(expectedOutput, Integer.parseInt(output.trim()));
    }

    /*
    let
        var increment:int = 1;
        fun g:int(x:bool)
            x + increment;

        fun f:int(y:(int) -> int)
            y(3);
    in
        print(f(g));
     */
    @Test
    public void testWrongParameterContravariance() throws IOException {
        String code = "let\n" +
                "    var increment:int = 1;\n" +
                "    fun g:int(x:bool)\n" +
                "        x + increment;\n" +
                "\n" +
                "    fun f:int(y:(int)->int)\n" +
                "        y(3);\n" +
                "in\n" +
                "    print(f(g));";

        assertError(code, "Wrong type of parameter for function invocation f");
    }

    /*  let
            fun isMaxOrEqual:bool(x:int, y:int)
                x >= y;
            fun f:int(y:(bool, bool) -> bool)
                y(true, false);
            var result:int = f(isMaxOrEqual);
        in
            print(result); */
    @Test
    public void testOkReturnCovariance() throws IOException {
        String code = "let\n" +
                "    fun isMaxOrEqual:bool(x:int, y:int)\n" +
                "        x >= y;\n" +
                "\n" +
                "    fun f:int(y:(bool, bool) -> bool)\n" +
                "        y(true, false);\n" +
                "    \n" +
                "    var result:int = f(isMaxOrEqual);\n" +
                "in\n" +
                "    print(result);";

        /* True is 1 and false is 0, 1 is greater than 0. The result expected is the int 1 */
        int expectedOutput = 1;
        String output = TestUtils.execute(code);
        Assert.assertEquals(expectedOutput, Integer.parseInt(output.trim()));
    }

    /*  let
            fun sum:int(x:int, y:int)
                x + y
            fun f:bool(y:(bool, bool) -> bool)
                y(true, false);
        in
            print(f(sum)); */
    @Test
    public void testWrongReturnCovariance() throws IOException {
        String code = "let\n" +
                "    fun sum:int(x:int, y:int)\n" +
                "        x + y;\n" +
                "    fun f:bool(y:(bool, bool) -> bool)\n" +
                "        y(true, false);\n" +
                "in\n" +
                "    print(f(sum));";

        assertError(code, "Wrong type of parameter for function invocation f");
    }
}
