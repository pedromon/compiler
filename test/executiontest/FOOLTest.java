package executiontest;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import utils.TestUtils;

import java.io.IOException;
import java.util.Arrays;
import java.util.stream.Collectors;

@RunWith(JUnit4.class)
public class FOOLTest {

    @Test
    public void bankloan() throws IOException {
        String code = "let\n" +
                "\n" +
                "  class Account (money:int) {\n" +
                "    fun getMon:int () money;\n" +
                "  }\n" +
                "  \n" +
                "  class TradingAcc extends Account (invested:int) {\n" +
                "    fun getInv:int () invested;\n" +
                "  }\n" +
                "  \n" +
                "  class BankLoan (loan: Account) {\n" +
                "    fun getLoan:Account () loan;\n" +
                "    fun openLoan:Account (m:TradingAcc) if ((m.getMon()+m.getInv())>=30000) \n" +
                "      then {new Account(loan.getMon())} \n" +
                "      else {null};\n" +
                "  } \n" +
                "  \n" +
                "  class MyBankLoan extends BankLoan (loan: TradingAcc) {\n" +
                "    fun openLoan:TradingAcc (l:Account) if (l.getMon()>=20000) \n" +
                "      then {new TradingAcc(loan.getMon(),loan.getInv())} \n" +
                "      else {null};\n" +
                "  } \n" +
                "    \n" +
                "  var bl:BankLoan = new MyBankLoan(new TradingAcc(50000,40000));\n" +
                "  var myTradingAcc:TradingAcc = new TradingAcc(20000,5000);\n" +
                "  var myLoan:Account = bl.openLoan(myTradingAcc);\n" +
                "      \n" +
                "in print(if (myLoan==null) then {0} else {myLoan.getMon()});  \n";

        String output = TestUtils.execute(code);
        Assert.assertEquals(50000, Integer.parseInt(output.trim()));
    }

    @Test
    public void linsum() throws IOException {
        String code = "let\n" +
                "  fun g:int(x:(int,int)->int)\n" +
                "    x(5,7);\n" +
                "  fun f:int(c:int)\n" +
                "    let\n" +
                "      fun linsum:int(a:int,b:int)\n" +
                "        (a+b)*c;\n" +
                "    in \n" +
                "      g(linsum);   \n" +
                "in\n" +
                "  print(f(2));";

        String output = TestUtils.execute(code);
        Assert.assertEquals(24, Integer.parseInt(output.trim()));
    }

    @Test
    public void quicksort() throws IOException {
        String code = "let\n" +
                "\n" +
                "  class List (f:int, r:List) {\n" +
                "    fun first:int() f;\n" +
                "    fun rest:List() r; \n" +
                "  }\n" +
                "  \n" +
                "  fun printList:List (l:List) \n" +
                "      let  \n" +
                "        fun makeList:List (l:List, i:int) new List (i,l);\n" +
                "      in \n" +
                "         if (l == null) \n" +
                "           then {null}\n" +
                "           else {makeList(printList(l.rest()),print(l.first()))};                 \n" +
                "  \n" +
                "  fun append:List (l1:List, l2:List)\n" +
                "      if (l1 == null)\n" +
                "        then {l2}\n" +
                "        else {new List(l1.first(), append(l1.rest(),l2))} ;  \n" +
                "    \n" +
                "  /* filtra la lista \"l\" mantenendo solo gli elementi */\n" +
                "  /* che sono: <= a \"pivot\", se \"before\" � true       */\n" +
                "  /*            > a \"pivot\", se \"before\" � false      */\n" +
                "  fun filter:List (l:List, pivot:int, before:bool) \n" +
                "      let\n" +
                "        fun accept:bool (cond:bool)\n" +
                "            if (before) then {cond} else {!(cond)};\n" +
                "      in        \n" +
                "        if (l == null) \n" +
                "          then {null}\n" +
                "          else {if ( accept(l.first()<=pivot) )\n" +
                "                  then { new List( l.first(), filter(l.rest(),pivot,before) ) }\n" +
                "                  else { filter(l.rest(),pivot,before) }\n" +
                "               };    \n" +
                "  \n" +
                "  fun quicksort:List (l:List)    \n" +
                "      let\n" +
                "        var pivot:int = if (l==null) then {0} else {l.first()}; \n" +
                "      in          \n" +
                "        if (l == null) \n" +
                "           then {null}\n" +
                "           else {append( \n" +
                "                   quicksort( filter(l.rest(),pivot,true) ),\n" +
                "                   new List(  pivot,  quicksort( filter(l.rest(),pivot,false) )  )                       \n" +
                "                 )};\n" +
                "                                                        \n" +
                "  var l:List = new List (2, \n" +
                "                    new List(1,\n" +
                "                        new List(4,\n" +
                "                            new List (3, \n" +
                "                                new List(2,\n" +
                "                                    new List(5,null))))));\n" +
                "  \n" +
                "in printList(quicksort(l));\n" +
                "\n";


        String output = TestUtils.execute(code);

        Assert.assertEquals(
                Arrays.asList(1,2,2,3,4,5),
                Arrays.stream(output.trim().split("\n")).map(s -> s.charAt(0))
                        .filter(Character::isDigit).map(c -> Integer.parseInt(c + "")).collect(Collectors.toList())
        );
    }

    @Test
    public void quicksort_ho() throws IOException {
        String code = "let\n" +
                "\n" +
                "  class List (f:int, r:List) {\n" +
                "    fun first:int() f;\n" +
                "    fun rest:List() r; \n" +
                "  }\n" +
                "  \n" +
                "  fun printList:List (l:List) \n" +
                "      let  \n" +
                "        fun makeList:List (l:List, i:int) new List (i,l);\n" +
                "      in \n" +
                "         if (l == null) \n" +
                "           then {null}\n" +
                "           else {makeList(printList(l.rest()),print(l.first()))};                 \n" +
                "  \n" +
                "  fun append:List (l1:List, l2:List)\n" +
                "      if (l1 == null)\n" +
                "        then {l2}\n" +
                "        else {new List(l1.first(), append(l1.rest(),l2))} ;  \n" +
                "    \n" +
                "  fun filter:List (l:List, accept:(int)->bool) \n" +
                "      if (l == null) \n" +
                "        then {null}\n" +
                "        else {if ( accept(l.first()) )\n" +
                "                then { new List( l.first(), filter(l.rest(),accept) ) }\n" +
                "                else { filter(l.rest(),accept) }\n" +
                "             };\n" +
                "         \n" +
                "  fun quicksort:List (l:List, rel:(int,int)->bool)    \n" +
                "      let\n" +
                "        var pivot:int = if (l==null) then {0} else {l.first()};       \n" +
                "        fun beforePivot:bool (x:int) rel(x,pivot);\n" +
                "        fun afterPivot:bool (x:int) !(rel(x,pivot));\n" +
                "      in        \n" +
                "         if (l == null) \n" +
                "           then {null}\n" +
                "           else {append( \n" +
                "                   quicksort( filter(l.rest(),beforePivot), rel ),                  \n" +
                "                   new List(  pivot,  quicksort( filter(l.rest(),afterPivot), rel )  )                       \n" +
                "                 )};\n" +
                "                                   \n" +
                "  fun inc:bool (x:int,y:int) x<=y;\n" +
                "  fun dec:bool (x:int,y:int) x>=y;\n" +
                "               \n" +
                "  var l:List = new List (2, \n" +
                "                    new List(1,\n" +
                "                        new List(4,\n" +
                "                            new List (3, \n" +
                "                                new List(2,\n" +
                "                                    new List(5,null))))));\n" +
                "  \n" +
                "in printList(quicksort(l,dec));\n" +
                "\n";

        String output = TestUtils.execute(code);

        Assert.assertEquals(
                Arrays.asList(5,4,3,2,2,1),
                Arrays.stream(output.trim().split("\n")).map(s -> s.charAt(0))
                        .filter(Character::isDigit).map(c -> Integer.parseInt(c + "")).collect(Collectors.toList())
        );
    }
}
