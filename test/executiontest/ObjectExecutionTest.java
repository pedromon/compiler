package executiontest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class ObjectExecutionTest extends AbstractExecutionTest {

    /*
    let
        class Account (balance:int) {
            fun m:int() balance;
        }

        class Account2 extends Account(balance:bool) {
            fun m:bool() balance;
        }

        var x:Account = new Account2(true);
    in print(x.m());
     */
    @Test
    public void test1() {
        String code = "let\n" +
                "        class Account (balance:int) {\n" +
                "            fun m:int() balance;\n" +
                "        }\n" +
                "\n" +
                "        class Account2 extends Account(balance:bool) { \n" +
                "            fun m:bool() balance;\n" +
                "        }\n" +
                "\n" +
                "        var x:Account = new Account2(true);\n" +
                "    in print(x.m());";

        assertNumeric(code, 1, (out) -> Integer.parseInt(out.trim()));
    }

    /*
let
    class A (balance:int) {
        fun m:int() balance;
    }

    class B extends A(c:bool, balance:bool) {
        fun m:bool()
            balance;
    }

    class C extends B() { }

    class F extends C() {}
    class H extends C() {}

    var x:A = if(true) then {new C(true, true)} else { new B(true, true)};
    var y:C = if(true) then {new F(true, true)} else { new H(true, true)};

in y;
 */
    @Test
    public void testLowestCommonAncestor() {
        String code = "let\n" +
                "    class A (balance:int) {\n" +
                "        fun m:int() balance;\n" +
                "    }\n" +
                "\n" +
                "    class B extends A(c:bool, balance:bool) {\n" +
                "        fun m:bool()\n" +
                "            balance;\n" +
                "    }\n" +
                "\n" +
                "    class C extends B() { }\n" +
                "\n" +
                "    class F extends C() {}\n" +
                "    class H extends C() {}\n" +
                "\n" +
                "    var x:A = if(true) then {new C(true, true)} else { new B(true, true)};\n" +
                "    var y:C = if(true) then {new F(true, true)} else { new H(true, true)};\n" +
                "\n" +
                "in print(y.m());";

        assertNumeric(code, 1, (out) -> Integer.parseInt(out.trim()));
    }
}
