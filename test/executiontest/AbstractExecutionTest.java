package executiontest;

import org.junit.Assert;
import utils.TestUtils;

import java.io.IOException;
import java.util.function.Function;

public abstract class AbstractExecutionTest {

    protected void assertNumeric(String code, Integer expected, Function<String, Integer> convert) {
        try {
            String output = TestUtils.execute(code);
            Assert.assertEquals(expected, convert.apply(output));
        } catch (IOException e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

    protected void assertError(String code, String expectedErrorMessage) {
        try {
            TestUtils.execute(code);
            Assert.fail();
        } catch (RuntimeException e) {
            System.out.println(e.getMessage());
            Assert.assertEquals(expectedErrorMessage, e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

}
