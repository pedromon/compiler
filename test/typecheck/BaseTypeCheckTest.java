package typecheck;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import type.IntType;

@RunWith(JUnit4.class)
public class BaseTypeCheckTest extends AbstractTypeCheckTest {

    @Test
    public void test1() {
        String code = "let\n" +
                "  var y:int = 5+2;\n" +
                "  fun f:bool (n:int, m:int)\n" +
                "    let \n" +
                "      var x:int = m;\n" +
                "  in x==y;   \n" +
                "in  \n" +
                "  print ( \n" +
                "    if f(y,8) \n" +
                "      then { false }\n" +
                "      else { 10 }\n" +
                "  ); ";

        assertType(code, IntType.TYPE);
    }


    @Test
    public void test2() {
        String code = "let\n" +
                "  var y:int = 5+true;\n" +
                "  fun f:bool (n:int, m:int)\n" +
                "    let \n" +
                "      var x:int = m;\n" +
                "  in x==y;   \n" +
                "in  \n" +
                "  print ( \n" +
                "    if f(y,true) \n" +
                "      then { false }\n" +
                "      else { 10 }\n" +
                "  ); ";
        assertType(code, IntType.TYPE);
    }

    @Test
    public void test3() {
        String code = "let\n" +
                "  var y:int = 5+2;\n" +
                "  fun f:int (n:int, m:int)\n" +
                "    let \n" +
                "      var x:int = m;\n" +
                "  in x+y;   \n" +
                "in  \n" +
                "  print ( \n" +
                "    if f(y,8) \n" +
                "      then { false }\n" +
                "      else { 10 }\n" +
                "  ); ";

        assertTypeError(code,"No boolean condition inside if.");
    }

    @Test
    public void test4() {
        String code = "let\n" +
                "  var y:int = 5+3;\n" +
                "  fun f:bool (n:bool, m:int)\n" +
                "    let\n" +
                "      var x:int = m;\n" +
                "  in x==y;\n" +
                "in\n" +
                "  print (\n" +
                "    if f(y,8)\n" +
                "      then { false }\n" +
                "      else { 10 }\n" +
                "  );";

        assertTypeError(code,"Wrong type of parameter for function invocation f");
    }

    /*
    let
      var y:int = 5+3;
      fun f:int (n:int, m:int)
        let
          var x:int = m;
      in x+y;
      var result:bool = f(y, 2);
    in
      print (result);
     */
    @Test
    public void test5() {
        String code = "let\n" +
                "  var y:int = 5+3;\n" +
                "  fun f:int (n:int, m:int)\n" +
                "    let\n" +
                "      var x:int = m;\n" +
                "  in x+y;\n" +
                "  var result:bool = f(y, 2);\n" +
                "in\n" +
                "  print (result);";

        assertTypeError(code,"Incompatibles assignment for var result");
    }

}
