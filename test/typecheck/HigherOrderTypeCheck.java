package typecheck;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import type.ArrowType;
import type.BoolType;
import type.IntType;

import java.util.Arrays;

@RunWith(JUnit4.class)
public class HigherOrderTypeCheck extends AbstractTypeCheckTest {


    @Test
    public void testOkParameterContravariance() {
        String code = "let\n" +
                "    var increment:int = 1;\n" +
                "    fun g:int(x:int)\n" +
                "        x + increment;\n" +
                "\n" +
                "    fun f:int(y:(bool)->int)\n" +
                "        y(true);\n" +
                "in\n" +
                "    print(f(g));";

        assertType(code, IntType.TYPE);
    }

    @Test
    public void testWrongParameterContravariance() {
        String code = "let\n" +
                "    var increment:int = 1;\n" +
                "    fun g:int(x:bool)\n" +
                "        x + increment;\n" +
                "\n" +
                "    fun f:int(y:(int)->int)\n" +
                "        y(3);\n" +
                "in\n" +
                "    print(f(g));";

        assertTypeError(code, "Wrong type of parameter for function invocation f");
    }

    /*  let
            fun isMaxOrEqual:bool(x:int, y:int)
                x >= y;
            fun f:int(y:(bool, bool) -> bool)
                y(true, false);
            var result:int = f(isMaxOrEqual);
        in
            print(result); */
    @Test
    public void testOkReturnCovariance() {
        String code = "let\n" +
                "    fun isMaxOrEqual:bool(x:int, y:int)\n" +
                "        x >= y;\n" +
                "\n" +
                "    fun f:int(y:(bool, bool) -> bool)\n" +
                "        y(true, false);\n" +
                "    \n" +
                "    var result:int = f(isMaxOrEqual);\n" +
                "in\n" +
                "    print(result);";

        assertType(code, IntType.TYPE);
    }

    /*  let
            fun sum:int(x:int, y:int)
                x + y
            fun f:bool(y:(bool, bool) -> bool)
                y(true, false);
        in
            print(f(sum)); */
    @Test
    public void testWrongReturnCovariance() {
        String code = "let\n" +
                "    fun sum:int(x:int, y:int)\n" +
                "        x + y;\n" +
                "    fun f:bool(y:(bool, bool) -> bool)\n" +
                "        y(true, false);\n" +
                "in\n" +
                "    print(f(sum));";
        assertTypeError(code, "Wrong type of parameter for function invocation f");
    }

    /*
    let

        fun g:int(a:bool, c:bool)
            a + 1;

        fun f:bool(b:int, z:bool)
            if(b == 0) then { true } else { false };

        var x:(bool, bool) -> int = if(true) then { f } else { g };
    in
        print(x);
     */
    @Test
    public void testLowestCommonAncestor() {
        String code = "let\n" +
                "\n" +
                "    fun g:int(a:bool, c:bool)\n" +
                "        a + 1;\n" +
                "\n" +
                "    fun f:bool(b:int, z:bool)\n" +
                "        if(b == 0) then { true } else { false };\n" +
                "\n" +
                "    var x:(bool, bool) -> int = if(true) then { f } else { g };\n" +
                "in\n" +
                "    print(x);";

        /* Expected type (bool, bool) -> int */
        assertType(code, new ArrowType(Arrays.asList(BoolType.TYPE, BoolType.TYPE), IntType.TYPE));
    }

    /*
    let

        fun g:int(a:bool, c:bool)
            a + 1;

        fun f:bool(b:int, z:bool)
            if(b == 0) then { true } else { false };

        var x:(int, int) -> int = if(true) then { f } else { g };
    in
        print(x);
     */
    @Test
    public void testWrongLowestCommonAncestor() {
        String code = "let\n" +
                "\n" +
                "    fun g:int(a:bool, c:bool)\n" +
                "        a + 1;\n" +
                "\n" +
                "    fun f:bool(b:int, z:bool)\n" +
                "        if(b == 0) then { true } else { false };\n" +
                "\n" +
                "    var x:(int, int) -> int = if(true) then { f } else { g };\n" +
                "in\n" +
                "    print(x);";

        /* Expected type (bool, bool) -> int */
        assertTypeError(code,"Incompatibles assignment for var x");
    }

    /*
    let
        class A(u:int) {
            fun getU:int()
                u;
        }

        fun g:int(a:A)
            a.getU() + 1;

        fun f:bool(b:int)
            if(b == 0) then { true } else { false };

        var x:(A) -> int = if(true) then { f } else { g };
    in
        print(x);
     */
    @Test
    public void testNonExistLowestCommonAncestor() {
        String code = "let\n" +
                "    class A(u:int) {\n" +
                "        fun getU:int()\n" +
                "            u;\n" +
                "    }\n" +
                "\n" +
                "    fun g:int(a:A)\n" +
                "        a.getU() + 1;\n" +
                "\n" +
                "    fun f:bool(b:int)\n" +
                "        if(b == 0) then { true } else { false };\n" +
                "\n" +
                "    var x:(A) -> int = if(true) then { f } else { g };\n" +
                "in\n" +
                "    print(x);";

        /* Expected type (bool, bool) -> int */
        assertTypeError(code, "Invalid if type");
    }
}
