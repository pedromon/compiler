package typecheck;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import test.FOOLLexer;
import test.FOOLParser;
import type.IntType;
import type.RefType;
import type.Type;

import java.sql.Ref;

@RunWith(JUnit4.class)
public class ObjectTypeCheckTest extends AbstractTypeCheckTest {

    @Test
    public void test1() {
        String code = "let\n" +
                "\n" +
                "  class Account (balance:int) {\n" +
                "    fun m:bool()\n" +
                "        true;\n" +
                "  }\n" +
                "\n" +
                "  var x:Account = new Account(5);\n" +
                "\n" +
                "in print(x);\n";


        assertType(code, RefType.of("Account"));
    }

    /*
    let
        class Account (balance:int) {
            fun m:bool() true;
        }

        class Account2 extends Account() { }

        var x:Account = new Account2(5);
    in print(x);
     */
    @Test
    public void test2() {
        String code = "let\n" +
                "    class Account (balance:int) {\n" +
                "        fun m:bool() true;\n" +
                "    }\n" +
                "\n" +
                "    class Account2 extends Account() { }\n" +
                "\n" +
                "    var x:Account = new Account2(5);\n" +
                "in print(x);";

        assertType(code, RefType.of("Account"));
    }

    /*
    let
        class A (balance:int) {
            fun m:int() balance;
        }

        class B extends A(c:bool, balance:bool) {
            fun m:bool()
                balance;
        }

        var x:A = new B(true, false);

    in x;
     */
    @Test
    public void testOverride() {
        String code = "let\n" +
                "    class A (balance:int) {\n" +
                "        fun m:int() balance;\n" +
                "    }\n" +
                "\n" +
                "    class B extends A(c:bool, balance:bool) {\n" +
                "        fun m:bool()\n" +
                "            balance;\n" +
                "    }\n" +
                "\n" +
                "    var x:A = new B(true, false);\n" +
                "\n" +
                "in x;";

        assertType(code, RefType.of("A"));
    }

    /*
    let
        class A (balance:int) {
            fun m:int() balance;
        }

        class B extends A(c:bool, balance:bool) {
            fun m:bool()
                balance;
        }

        class C extends B() { }

        class F extends C() {}
        class H extends C() {}

        var x:A = if(true) then {new C(true, true)} else { new B(true, true)};
        var y:C = if(true) then {new F(true, true)} else { new H(true, true)};

    in y;
     */
    @Test
    public void testLowestCommonAncestor() {
        String code = "let\n" +
                "    class A (balance:int) {\n" +
                "        fun m:int() balance;\n" +
                "    }\n" +
                "\n" +
                "    class B extends A(c:bool, balance:bool) {\n" +
                "        fun m:bool()\n" +
                "            balance;\n" +
                "    }\n" +
                "\n" +
                "    class C extends B() { }\n" +
                "\n" +
                "    class F extends C() {}\n" +
                "    class H extends C() {}\n" +
                "\n" +
                "    var x:A = if(true) then {new C(true, true)} else { new B(true, true)};\n" +
                "    var y:C = if(true) then {new F(true, true)} else { new H(true, true)};\n" +
                "\n" +
                "in y;";

        assertType(code, RefType.of("C"));
    }

    /*
    let
        class A (balance:int) {
            fun m:int() balance;
        }

        class B extends A(c:bool, balance:bool) {
            fun m:bool()
                balance;
        }

        class C extends B() { }

        class F extends C() {}
        class H() { }

        var x:A = if(true) then {new C(true, true)} else { new H()};

    in x;
     */
    @Test
    public void testWrongLowestCommonAncestor() {
        String code = "let\n" +
                "    class A (balance:int) {\n" +
                "        fun m:int() balance;\n" +
                "    }\n" +
                "\n" +
                "    class B extends A(c:bool, balance:bool) {\n" +
                "        fun m:bool()\n" +
                "            balance;\n" +
                "    }\n" +
                "\n" +
                "    class C extends B() { }\n" +
                "\n" +
                "    class F extends C() {}\n" +
                "    class H() { }\n" +
                "\n" +
                "    var x:A = if(true) then {new C(true, true)} else { new H()};\n" +
                "\n" +
                "in x;";

        assertTypeError(code, "Invalid if type");
    }

}
