package typecheck;

import org.junit.Assert;
import test.FOOLParser;
import type.IntType;
import type.Type;
import utils.TestUtils;

public class AbstractTypeCheckTest {

    protected void assertType(String code, Type expectedType) {

        FOOLParser parser = TestUtils.getParser(code);

        Type type = parser.prog().ast.typeCheck();

        Assert.assertEquals(expectedType, type);
    }

    protected void assertTypeError(String code, String expectedError) {
        try {
            FOOLParser parser = TestUtils.getParser(code);
            Type type = parser.prog().ast.typeCheck();
            Assert.assertFalse(false);
        } catch (RuntimeException e) {
            System.out.println(e.getMessage());
            Assert.assertEquals(expectedError, e.getMessage());
        }
    }

}
