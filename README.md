### Note di progetto

1. I tipi booleani sono sottotipi di interi. E' possibile quindi effettuare operazioni tra interi e booleani, ad esempio 5 + true, che è di tipo IntType, ovvero il tipo più generale.

2. L'interfaccia Type definisce il comportamento di ogni tipo e ogni classe che la implementa definirà le relazioni di sottotipo, attraverso l'implementazione del metodo isSubtype(). 
Esempio IntType:
```java
  public class IntType {
    // ....
    public boolean isSubtype(Type type2) {
        return (type2 instanceof IntType);
    }
  }
  
  public class BoolType {
    // ....
    public boolean isSubtype(Type type2) {
        if(type2 instanceof BoolType)
            return true;
        else if(type2 instanceof IntType) // controllo se è sottotipo di intero
            return true;
        
        // non è ne sottotipo di int, ne bool.
        return false;
    }
  }
  ```
  Di seguono vengono mostrati i tipi esistenti attraverso un diagramma UML:<br>
  ![](diagram/Type_class_img.png)
  
3. Nell'implementazione delle classi relative ai vari nodi si è cercato 
il più possibile di mettere in relazione tra loro nodi con comportamenti simili, suddividendoli in tre macro-categorie.<br>
Grazie a queste scelte l'architettura risulta ben organizzata e maggiormente riusabile.
Negli UML di seguito vengono mostrate le relazioni tra i nodi:<br>

#### SingleNode:
  ![](diagram/SingleNode_class_img.png)
#### BinaryNode:
  ![](diagram/BinaryNodeAndSub_class_img.png)
#### TrieNode


  #### Progettazione Layout

  | AR semplificato per ambiente globale (main) | AR per funzioni completo |
  :-------------------------:|:-------------------------:
  ![](docs/ar-main.png) | ![](docs/ar-generico.png)
    

  ## Higher order
  Cambio offset, ora ogni tipo funzionale occupa due offset. E' necessario in fase di parsing sistemare gli offset in caso di tipo funzionale.
  Rispetto all'offset che salvo in Symbol Table, ho che:
  
  | Offset | Contenuto |
  |-------| ----- |
  | offset in ST Entry | Frame pointer all'AR in cui e' dichiarata la funzione |
  | offset in ST Entry - 1 | Etichetta della funzione |
  
  Offset doppio: <br>
  ![](docs/hg-arrowtype.png)
  
  In pratica ogni volta che si incontra un tipo funzione, che sia dicharazione di funzione o dichiarazione di variabile funzionale o parametro, l'offset 
  deve essere incrementato una volta in piu, proprio per occupare offset doppio, in modo tale che un' eventuale dichiarazione successiva si trovi all'offset corretto.
  Particolare attenzione va ai parametri che nella versione non higher order partono con offset a 1, mentre in higher order se il primo parametro e' di tipo ArrowType,
  l'offset iniziale viene fatto partitre a 2, in modo tale che all' offset messo in Symbol table corrisponda l'AR in cui e' dichiarata la funzione passata come parametro e che a offset-1
  vi sia l'etichetta della funzione passata. Questa immagine mostra sia dichiarazioni che parametri:
  
  ![](docs/hg-ar-example.png)
  
  
  !!! Importante: A cosa serve l'fp all'AR in cui e' dichiarata la funzione? Serve per poter accedere a quel AR in cui potrebbero esserci
  variabili locali che la funzione utilizza, o peggio ancora la funzione potrebbe essere locale e non visibile da scope esterno. Ad esempio:
  ```
  let
    fun g:int(y:(int) -> int)
        y(3);
    fun f:int()
        let
            var z:int = 1;
            fun h:int(a:int)
                a + z;
        in
            g(h);
  in
    f();
  ```
  Senza il frame pointer all'AR, `g` chiama la funzione `y` che gli e' stata passata, ma in realta' `y` sarebbe la funzione `h` che non e' visibile dalla scope di `g` e tanto meno non e' visibile `z` che viene usata
  da `h`, e' quindi necessario avere l'accesso all'AR in cui `h` e' stata dichiarata.
  #### Type Check
  Controllo sui tipi arrow type, varianza e controvarianza. In particolare preso un arrow type `t1` e un arrow type `t2`,
  `t1` e' sottotipo di `t2`, se vi e' una relazione di varianza sui tipi di ritorno e una relazione di controvarianza sui parametri.
  Esempio:
  ```
  let
    fun g:bool(y:int)               ArrowType di g: (int) -> bool
    fun f:int(x:(bool) -> int)      ArrowType di x: (bool) -> int
        x(true);
  in
    f(g);
  ```
  Cio e' possibile `f(g)`, perche' `g` e' sottotipo del tipo di `x`, ovvero sottotipo dell'arrow type `(bool) -> int`, infatti
  il c'e' relazione di varianza sul tipo di ritorno, ovvero il tipo `bool` di ritorno di `g` e' sottoipo del tipo `int` che `x` dichiara essere tipo di ritorno.
  Vale invece controvarianza sui parametri, infatti, il tipo `int` del parametro di `g` e' sopratipo del tipo `bool` di `x`.
  
  Un caso analogo, ma in cui il typecheck fallisce proprio perche' non sono rispettate varianza e contro varianza, e' il seguente:
  ```
  let
    fun g:int(y:bool)               ArrowType di g: (bool) -> int
    fun f:int(x:(int) -> int)       ArrowType di x: (int) -> int
        x(4);
  in
    f(g)
  ``` 
  Questo caso provoca che la chiamata di `g` che vuole `bool`, avviene passando un `int`, ma `int` non e' sottotipo di `bool` (viceversa si). 
  
  ## Object extension
  Si introduce lo heap e in particolare la dispatch table. Ci sono due layout, uno per allocazione oggetti e uno per dispatch table
  
  Layout allocazione oggetti:
  ![](docs/layout-object.png)
  
  Layout dispatch table:
  ![](docs/layout-dispatch.png)
  
  ### Parser
  1. ClassNode: rappresenta dichiarazione di una classe.
  2. FieldNode: rappresenta dichiarazione di un campo della classe.
  3. MethodNode: rappresenta dichiarazione metodo di una classe.
  4. ClassCallNode: chiamata a metodo da un'oggeto es: x.f(5)
  5. NewNode: instanziazione di una classe es. new A(3,4,true);
  6. EmptyNode: rappresenta null.
  7. RefType: tipo riferimento, contiene nome classe. Es: var x:A = new A(), x e' di tipo RefType.
  8. EmptyType: il tipo di null.
  9. ClassType: tipo della dichirazione di una classe, contiene lista dei tipi dei campi e dei metodi (arrow type). Il tipo di class node.
 
  Quando il parser e' all'interno della dichiarazione di una classe, la symbol table deve contenere tutti i simboli della classe,
  quindi metodi e campi, anche quelli ereditati, viene quindi creata la VirtualTable.
  Questa permette preservare le dichiarazioni interne ad una classe quando la symbol table viene distrutta all'uscita del nesting level.
  
  Una virtual table è come una symbol table:   ``` Map<String, STEntry>  virtualTable``` . Un'insieme di virtual table, una
  per ogni classe viene definita class table. Questa contiene le virtual table per ogni classe dichiarata.
  
  Class Table:   ``` Map<String, Map<String, STEntry>> classTable ``` 
  
  