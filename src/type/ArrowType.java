package type;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ArrowType extends Type{

    private List<Type> parList;//contiene i tipi dei parametri es: (tipo1,tipo2...)
    private Type retType;//-> tipo di ritorno

    public ArrowType(Type retType) {
        this.parList = new ArrayList<>();
        this.retType = retType;
    }

    public ArrowType(List<Type> parList, Type retType) {
        this.parList = parList;
        this.retType = retType;
    }

    @Override
    protected String typePrint() {
        return "ArrowType";
    }

    @Override
    public boolean isSubtype(Type type) {
        if(!(type instanceof ArrowType)){
            return false;
        }else{
            ArrowType arrowType = (ArrowType) type;
            if((arrowType.getParList().size()==parList.size())){
                boolean controvarianza = IntStream.range(0, parList.size()).allMatch(i -> arrowType.parList.get(i).isSubtype((parList.get(i))));
                return controvarianza && retType.isSubtype(arrowType.retType);
            }
            return false;
        }

    }

    @Override
    public String toPrint(String indent) {
        return super.toPrint(indent) + parList.stream().map(p->p.toPrint(indent+"  ")).collect(Collectors.joining())
                + retType.toPrint(indent + "  ->");
    }

    public List<Type> getParList() {
        return parList;
    }

    public void setParList(List<Type> parList) {
        this.parList = parList;
    }

    public void addParToList(Type type){
        parList.add(type);
    }

    public Type getRetType() {
        return retType;
    }

    public void setRetType(Type retType) {
        this.retType = retType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ArrowType arrowType = (ArrowType) o;
        return Objects.equals(parList, arrowType.parList) &&
                Objects.equals(retType, arrowType.retType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(parList, retType);
    }
}
