package type;

public class EmptyType extends Type{

    @Override
    protected String typePrint() {
        return "EmptyType";
    }

    @Override
    public boolean isSubtype(Type type) {
        return type instanceof RefType;
        //sottotipo di ogni refType in questo modo
    }
}
