package type;

import node.interfaces.Printable;

public abstract class Type implements Printable {

    @Override
    public String toPrint(String indent) {
        return indent + typePrint() + "\n";
    }

    protected abstract String typePrint();

    public abstract boolean isSubtype(Type type);

}
