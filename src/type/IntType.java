package type;

public class IntType extends Type {

    public static final IntType TYPE =  new IntType();

    @Override
    protected String typePrint() {
        return "IntType";
    }

    @Override
    public boolean isSubtype(Type type) {
        return type instanceof IntType;
    }



}
