package type;

public class BoolType extends Type {

    @Override
    protected String typePrint() {
        return "BoolType";
    }

    @Override
    public boolean isSubtype(Type type) {
        return (type instanceof BoolType || type instanceof IntType);
    }

    public static final BoolType TYPE = new BoolType();
}
