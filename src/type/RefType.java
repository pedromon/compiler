package type;

import utils.InheritanceMap;

import java.util.Objects;

public class RefType extends Type {

    private String className;

    private RefType(String className){
        this.className = className;
    };

    public static RefType of(String className){
        return new RefType(className);
    }

    public String getClassName() {
        return className;
    }

    @Override
    protected String typePrint() {
        return "RefType@"+className;
    }

    @Override
    public boolean isSubtype(Type type) {
        if(type instanceof RefType){
            return InheritanceMap.getInstance().isSupertype(((RefType) type).className, this.className);
            //  var x:Account = new Account2(false, true);
            //this.className = Account2  ;  type.className = Account
        }else {
            return false;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RefType refType = (RefType) o;
        return className.equals(refType.className);
    }

    @Override
    public int hashCode() {
        return Objects.hash(className);
    }
}
