package type;

import node.interfaces.Node;
import node.object.FieldNode;
import node.object.MethodNode;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ClassType extends Type{

    List<Type> allFields; //tipi dei campi, inclusi quelli ereditati, in ordine di apparizione
    List<Type> allMethods; //tipi dei metodi, inclusi quelli ereditati, in ordine di apparizione

    public ClassType(){
        allFields = new ArrayList<>();
        allMethods= new ArrayList<>();
    }

    //class type nel caso in cui esendiamo una classe; prendiamo tutti i metodi e tutti i campi della classe estesa
    public ClassType(ClassType classToExtend){
        allFields = new ArrayList<>(classToExtend.allFields);
        allMethods = new ArrayList<>(classToExtend.allMethods);
    }

    @Override
    protected String typePrint() {
        return "ClassType";
    }

    @Override
    public boolean isSubtype(Type type) {
        if(!(type instanceof ClassType)){
            return false;
        }//in realtà questo controllo per come noi strutturiamo il g4 è superfluo
        ClassType superClassType = (ClassType) type;
        //facciamo uno stream di intero lungo quanto la lunghezza dei campi della superclasse (io sottoclasse avrò almeno i suoi campi e dovrò fare il controllo su quelli)
        IntStream.range(0, (superClassType.allFields.size())).forEach(i->{
            if(!allFields.get(i).isSubtype(superClassType.allFields.get(i))){
                //se la superclasse ha un campo bool ad esempio io non posso fare un override con un int
                //ovviamente questo discorso si applica anche alle classi perchè abbiamo il reftype che ha il suo isSubType
                throw  new RuntimeException("Wrong override of field "+i+"\n"+
                        "Expected:"+superClassType.allFields.get(i) + "or his subtype\n" +
                        "Found:"+allFields.get(i) + "\n");
            }
        });
        IntStream.range(0, (superClassType.allMethods.size())).forEach(i->{
            if(!allMethods.get(i).isSubtype(superClassType.allMethods.get(i))){
                //se la superclasse ha un campo bool ad esempio io non posso fare un override con un int
                throw  new RuntimeException("Wrong override of method "+i+"\n"+
                        "Expected:"+superClassType.allMethods.get(i) + "or his subtype\n" +
                                "Found:"+allMethods.get(i) + "\n");
            }
        });
        return true;
    }

    public boolean addFieldType(int index, Type fieldType) {
        if(index < allFields.size())
            allFields.set(index, fieldType);
        else
            allFields.add(fieldType);
        return true;
    }

    public boolean addMethodType(int index, Type methodType) {
        if(index < allMethods.size()) {
            allMethods.set(index, methodType);
        } else {
            allMethods.add(methodType);
        }
        return true;
    }

    public int getFieldsCount() {
        return allFields.size();
    }

    public int getMethodsCount() {
        return allMethods.size();
    }

    public List<Type> getAllFields() {
        return allFields;
    }

    public List<Type> getAllMethods() {
        return allMethods;
    }

    @Override
    public String toPrint(String indent) {
        return super.toPrint(indent) +
                allFields.stream().map(f -> f.toPrint(indent + DOUBLE_SPACE)).collect(Collectors.joining()) +
                allMethods.stream().map(m -> m.toPrint(indent + DOUBLE_SPACE)).collect(Collectors.joining());
    }


}
