package node;

import node.abstractnode.SingleNode;
import node.interfaces.Printable;
import node.tables.STEntry;
import type.ArrowType;
import type.ClassType;
import type.Type;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

/* Nodo per indicare gli usi di variabili */
public class IdNode extends SingleNode<String> {

    private int mNestingLevel;  /* Nesting level in cui ho l'uso */
    private STEntry mSTEntry;   /* Entry della Dichiarazione */

    public IdNode(String value, int nestingLevel, STEntry stEntry) {
        super(value);
        mNestingLevel = nestingLevel;
        mSTEntry = stEntry;
    }

    @Override
    public String toPrint(String indent) {
        return super.toPrint(indent) +
                indent + Printable.DOUBLE_SPACE + " at level: " + mNestingLevel + "\n" +
                mSTEntry.toPrint(indent + Printable.DOUBLE_SPACE);
    }

    @Override
    public String codeGen() {
        if(mSTEntry.getType() instanceof ArrowType){
            return "push "+mSTEntry.getOffset() + "\n"+
                    "lfp \n"+
                    IntStream.range(0, mNestingLevel- mSTEntry.getNestingLevel()).mapToObj(i->"lw\n").collect(Collectors.joining())+
                    "add\n"+
                    "lw\n"+ //recupera l'FP(indirizzo) dell AR IN cui è stata dichiarata e lo mette sullo stack
                    "push "+( mSTEntry.getOffset()-1) + "\n"+
                    "lfp \n"+
                    IntStream.range(0, mNestingLevel- mSTEntry.getNestingLevel()).mapToObj(i->"lw\n").collect(Collectors.joining())+
                    "add\n"+
                    "lw\n"; //recupero l'indirizzo dove è stata messa la codegen della funzione (etichetta) e lo mette sullo stack

        }else{
            return "push "+mSTEntry.getOffset() + "\n"+
                    "lfp \n"+
                    IntStream.range(0, mNestingLevel- mSTEntry.getNestingLevel()).mapToObj(i->"lw\n").collect(Collectors.joining())+
                    "add\n"+
                    "lw\n";

        /* pusho l'offset della dichiarazione della variabile, dopodichè
        con lfp carico il valore del frame pointer sulla cima dello stack
        risalgo la catena degli access link di un numero di volte pari alla differenza di nesting level tra il punto in cui la variabile viene usata e quello in cui viene dichiarata
        e alla fine della risalita viene caricato l'indirizzo dell access link corretto (di colui che contiene la dichiarazione)
        con la somma mi sposto di tante posizioni quanto è l offset (attenzione; l offset è negativo quindi mi sposto verso il basso)
        infine con lw carico il valore contenuto nell indirizzo di memoria finito sulla cima dello stack dopo le operazioni sopra citate
         */
        }

    }

    @Override
    public String getTypeName() {
        return "IdNode";
    }

    @Override
    public Type typeCheck() {

        if(mSTEntry.isMethod()) {
            throw new RuntimeException("Wrong usage of method identifier.");
        }else if(mSTEntry.getType() instanceof ClassType){
            throw new RuntimeException("Wrong usage of class identifier.");
            //controllo di non usare una classe come var.
            //se ho una class A e faccio A+3
        }
        return mSTEntry.getType();
    }
}
