package node;

import node.abstractnode.BinaryNode;
import node.interfaces.Node;
import type.BoolType;
import type.Type;
import utils.LabelGenerator;

public class OrNode extends BinaryNode {

    public OrNode(Node n1, Node n2) {
        super(n1, n2);
    }

    @Override
    public String codeGen() {
        String label = LabelGenerator.getInstance().generateLabel(LabelGenerator.Label.GENERIC);
        String labelTrue = LabelGenerator.getInstance().generateLabel(LabelGenerator.Label.GENERIC);

        return left.codeGen()+
                "push 1\n"+
                "beq " + labelTrue+ "\n"+
                right.codeGen()+
                "push 1\n"+
                "beq "+labelTrue +"\n"+
                "push 0 \n"+
                "b " + label+"\n"+
                labelTrue +":\n"+
                "push 1\n"+
                label+":"+"\n";

        /* valutiamo la codegen della prima espressione; se questa è vera (quindi = 1) saltiamo in un punto in cui pusheremo 1 in quanto l espressione è tutta vera;
            se questa è falsa (quindi 0) continuamo l esecuzione controllando se anche l altra sia non vera;
            se l'altra  è vera salto a labeltrue, dove andiamo a pushare un'1 in quanto tutta l espressione è valutata vera
           . se invece sono entrambe false pushamo uno 0 e per non andare a pushare uno 1 saltiamo incondizionatamente alla fine
         */
    }

    @Override
    public String getTypeName() {
        return "OrNode";
    }

    @Override
    public Type typeCheck() {
        if(left.typeCheck().equals(BoolType.TYPE) && right.typeCheck().equals(BoolType.TYPE)){
            return BoolType.TYPE;
        }else{
            throw new RuntimeException("Incompatible value for or operator");
        }
    }
}
