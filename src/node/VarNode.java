package node;

import node.interfaces.Node;
import type.Type;

/* Dichiarazione con assegnamento di una variabile (immutabile),
   estendiamo ParNode per riutilizzare sia il nome che il tipo. */
public class VarNode extends ParNode {

    private Node expNode;

    public VarNode(String id, Type type, Node expNode) {
        super(id, type);
        this.expNode = expNode;
    }

    @Override
    public String codeGen() {
        return expNode.codeGen();
    }

    @Override
    public String toPrint(String indent) {
        return super.toPrint(indent)
                + expNode.toPrint(indent + DOUBLE_SPACE);
    }

    @Override
    public String getTypeName() {
        return "VarNode";
    }

    public Node getExpNode() {
        return expNode;
    }

    public void setExpNode(Node expNode) {
        this.expNode = expNode;
    }

    @Override
    public Type typeCheck() {
        if(expNode.typeCheck().isSubtype(getType())){
            return getType();
        }else {
            throw new RuntimeException("Incompatibles assignment for var "+ getId());
        }
    }
}
