package node;

import node.interfaces.DeclarationNode;
import node.interfaces.Node;
import type.ArrowType;
import type.Type;
import utils.FunctionUtils;
import utils.LabelGenerator;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static node.interfaces.Printable.DOUBLE_SPACE;

/* Dichiarazione di una funzione, l'uso è dato da CallNode */
public class FunNode implements DeclarationNode {

    private String mId; //nome della funzione
    private Type mRetType; //tipo di ritorno
    private List<DeclarationNode> mDeclarationList; //dichiarazioni locali nel corpo della funzioni
    private List<ParNode> mParNodes; //
    private Node mExp;

    public FunNode(String id, Type retType) {
        mId = id;
        mRetType = retType;
        mDeclarationList = new ArrayList<>();
        mParNodes = new ArrayList<>();
    }

    public void addParam(ParNode node) {
        mParNodes.add(node);
    }

    public void addDeclarationList(List<DeclarationNode> decList) {
        mDeclarationList.clear();
        mDeclarationList.addAll(decList);
    }

    public void addBody(Node exp) {
        mExp = exp;
    }


    @Override
    public String codeGen() {

        String labelFunction = LabelGenerator.getInstance().generateFunctionLabel();

        FunctionUtils.addFunctionCode(functionCodeGen(labelFunction)); //uso una stringa gigante e globale per tenere tutte del codegen delle funzioni e metterle dopo halt

        return  "lfp\n" +//ogni funzione OCCUPA OFFSET DOPPIO!!!
                "push " + labelFunction + "\n"; //faccio una push per indicare che in questo punto c è una dichiarazione di funzione; la codegen della funzione dichiarata sarà dopo halt; ci servirà per saltare all etichetta

    }

    @Override
    public String getTypeName() {
        return "FunNode";
    }

    @Override
    public String toPrint(String indent) {
        return indent + getTypeName() + ":" + mId + "\n"
                + mRetType.toPrint(indent + DOUBLE_SPACE)
                + mParNodes.stream().map(p -> p.toPrint(indent + DOUBLE_SPACE)).collect(Collectors.joining())
                + mDeclarationList.stream().map(p -> p.toPrint(indent + DOUBLE_SPACE)).collect(Collectors.joining())
                + mExp.toPrint(indent + DOUBLE_SPACE);
    }

    @Override
    public Type typeCheck() {
        if (mExp.typeCheck().isSubtype(mRetType)) {
            mDeclarationList.forEach(d -> d.typeCheck()); //se nella let della funzione ho delle dichiarazioni devo andare a controllare se i loro assegnamenti sono corretti (es: dichiaro boolean x = 7 non va bene)
            return mRetType;
        } else {
            throw new RuntimeException("Wrong return type for function " + mId);
        }

    }

    protected String getId() {
        return mId;
    }

    @Override
    public Type getSymType() {
        return new ArrowType(mParNodes.stream().map(ParNode::getType).collect(Collectors.toList()), mRetType);
    }

    protected String functionCodeGen(String label) {
        return label + ":\n" + //label della funzione a cui saltare
                "cfp\n" + //copio il valore dello stack pointer e lo metto nel registro fp
                "lra\n" + //pusho il return address sullo stack
                mDeclarationList.stream().map(Node::codeGen).collect(Collectors.joining()) + //aggiungo la codegen di tutte le dichiarazioni
                mExp.codeGen() + //aggiungo la codegen dell espressione
                "srv\n" + //con srv prendo il valore in cima allo stack (risultato della codegen dell espressione) e lo metto nel registro rv (return value)
                mDeclarationList.stream().map(node->(node.getSymType() instanceof ArrowType) ? "pop\npop\n" : "pop\n").collect(Collectors.joining())+ //faccio una pop per ogni elemento della declist per cui ho fatto codegen (se è un arrow type faccio due pop)
                "sra\n" + //sra fa una pop del valore che ha sulla cima dello stack e lo mette dentro al registro  ra (il valore che al momento è sulla cima dello stack è quello che avevamo messo prima con lra)
                "pop\n" + //poppa il valore del Access Link (pushato prendentemente dal chiamante)
                mParNodes.stream().map(node->(node.getSymType() instanceof ArrowType) ? "pop\npop\n" : "pop\n").collect(Collectors.joining())+ //faccio una pop per ogni parametro per cui il chiamante ha fatto codeGen nel momento in cui costruiva l AR (se è arrowtype di pop ne faccio due)
                "sfp\n" + //in questo punto io ho il control link sulla cima dello stack . facendo sfp prendo il valore sulla cime dello stack (CL) e lo metto in FP, rispristinando L FP al chiamante
                "lrv\n" + //le operazioni sino a questo punto si trovano lo stack come prima della chiamata; lrv copia il valore che avevamo precedentemente messo in rv (code gen di exp) e aggiunge il suo valore di ritorno allo stack e quindi abbiamo rispettato l invariante
                "lra\n" + //carico il return address sulla cima dello stack in modo dal poterci saltare
                "js\n"; //prendo il valore che c è sulla cima dello stack (un indirizzo) e salto

    }

}
