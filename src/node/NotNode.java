package node;

import node.abstractnode.SingleNode;
import node.interfaces.Node;
import type.BoolType;
import type.Type;
import utils.LabelGenerator;

import java.util.function.Function;

public class NotNode extends SingleNode<Node> {

    public NotNode(Node value) {
        super(value);
    }

    @Override
    public String codeGen() {
        String labelExit = LabelGenerator.getInstance().generateLabel(LabelGenerator.Label.GENERIC);
        String label = LabelGenerator.getInstance().generateLabel(LabelGenerator.Label.GENERIC);

        return value.codeGen()+
                "push 0"+"\n"+
                "beq "+labelExit+"\n"+
                "push 0 \n"+
                "b "+label+"\n"+
                labelExit+":"+"\n"+
                "push 1\n"+
                label+":"+"\n";
    }

    @Override
    public String getTypeName() {
        return "NotNode";
    }

    @Override
    public Type typeCheck() {
        if(value.typeCheck().equals(BoolType.TYPE)){
            return BoolType.TYPE;
        }else{
            throw new RuntimeException("Incompatible values in Not");
        }
    }
}
