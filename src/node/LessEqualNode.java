package node;

import node.abstractnode.BinaryNode;
import node.interfaces.Node;
import type.BoolType;
import type.Type;
import utils.LabelGenerator;

public class LessEqualNode extends BinaryNode {

    public LessEqualNode(Node n1, Node n2) {
        super(n1, n2);
    }

    @Override
    public String codeGen() {
        String label = LabelGenerator.getInstance().generateLabel(LabelGenerator.Label.GENERIC);
        String labelExit = LabelGenerator.getInstance().generateLabel(LabelGenerator.Label.EXITLEQ);

        return left.codeGen()+
                right.codeGen()+
                "bleq "+ label + "\n"+
                "push 0\n"+//se arrivo qui significa che non sono minori uguali e non ho saltato
                "b "+labelExit+ " \n"+
                label+":"+"\n"+
                "push 1\n"+
                labelExit+ ":\n";
    }

    @Override
    public String getTypeName() {
        return "LessEqualNode";
    }

    @Override
    public Type typeCheck() {
        if(left.typeCheck().isSubtype(right.typeCheck()) || right.typeCheck().isSubtype(left.typeCheck())){
            return BoolType.TYPE;
        }else{
            throw new RuntimeException("Incompatibles type in less equals");
        }
    }
}
