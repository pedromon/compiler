package node;

import node.abstractnode.BinaryNode;
import node.interfaces.Node;
import type.IntType;
import type.Type;

public class MinusNode extends BinaryNode {

    public MinusNode(Node n1, Node n2) {
        super(n1, n2);
    }

    @Override
    public String codeGen() {
        return left.codeGen()
                + right.codeGen()
                + "sub\n";
    }

    @Override
    public String getTypeName() {
        return "MinusNode";
    }

    @Override
    public Type typeCheck() {
        if(left.typeCheck().isSubtype(IntType.TYPE) && right.typeCheck().isSubtype(IntType.TYPE)){
            return IntType.TYPE;
        }else{
            throw new RuntimeException("Incompatibles value in minus");
        }
    }
}
