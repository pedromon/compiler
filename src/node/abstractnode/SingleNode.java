package node.abstractnode;

import node.interfaces.Node;
import node.interfaces.Printable;

import java.util.function.Function;

public abstract class SingleNode<X> implements Node {

    protected X value;

    public SingleNode(X value){
        this.value = value;
    }

    @Override
    public String toPrint(String indent) {
        if(value instanceof Printable){
            return indent + getTypeName() + ":\n" + ((Printable) value).toPrint(indent + DOUBLE_SPACE) + "\n";
        }else{
            return indent + getTypeName() + ":" + value.toString() + "\n";
        }
    }
    //metodo che effettua la stampa: indentazione tipo : valore

    @Override
    public abstract String codeGen();

    protected X getValue(){
        return this.value;
    }


}
