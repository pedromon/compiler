package node.abstractnode;

import node.interfaces.Node;

public abstract class BinaryNode implements Node {

    protected Node left;
    protected Node right;

    public BinaryNode(Node n1, Node n2){
        this.left = n1;
        this.right = n2;
    }

    @Override
    public String toPrint(String indent) {
        return indent + getTypeName() + "\n" + left.toPrint(indent + DOUBLE_SPACE) + right.toPrint(indent + DOUBLE_SPACE);
    }


}
