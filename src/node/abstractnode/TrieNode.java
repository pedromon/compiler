package node.abstractnode;

import node.interfaces.Node;

public abstract class TrieNode implements Node {

    private Node left;
    private Node center;
    private Node right;

    public TrieNode(Node left, Node center, Node right) {
        this.left = left;
        this.center = center;
        this.right = right;
    }

    @Override
    public String codeGen() {
        return null;
    }

    @Override
    public String toPrint(String indent) {
        return indent + getTypeName() + ":\n" +
                center.toPrint(indent + DOUBLE_SPACE) +
                left.toPrint(indent + DOUBLE_SPACE) +
                right.toPrint(indent + DOUBLE_SPACE);
    }

    public Node getLeft() {
        return left;
    }

    public void setLeft(Node left) {
        this.left = left;
    }

    public Node getCenter() {
        return center;
    }

    public void setCenter(Node center) {
        this.center = center;
    }

    public Node getRight() {
        return right;
    }

    public void setRight(Node right) {
        this.right = right;
    }
}
