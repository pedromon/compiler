package node.object;

import node.interfaces.Node;
import type.EmptyType;
import type.Type;

public class EmptyNode implements Node {

    @Override
    public String codeGen() {
        return "push -1\n";
        //sicuramente diverso da object pointer di ogni oggetto creato
    }

    @Override
    public Type typeCheck() {
        return new EmptyType();
    }

    @Override
    public String getTypeName() {
        return "EmptyNode";
    }

    @Override
    public String toPrint(String indent) {
        return indent + getTypeName() + "(null)\n";
    }
}
