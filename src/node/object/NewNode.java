package node.object;

import node.interfaces.Node;
import node.tables.STEntry;
import svm.ExecuteVM;
import type.ArrowType;
import type.ClassType;
import type.RefType;
import type.Type;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class NewNode implements Node {

    private String mClassName;
    private STEntry mClassEntry;
    private List<Node> mParams;
    private int mNestingLevel;

    public NewNode(String className, STEntry classEntry, List<Node> params, int nestingLevel) {
        mClassName = className;
        mClassEntry = classEntry;
        mParams = params;
        mNestingLevel = nestingLevel;
    }

    @Override
    public String codeGen() {
        return mParams.stream().map(p->p.codeGen()).collect(Collectors.joining())+//abbiamo pushato la codegen di tutti i parametri mantenendo l invarianza
               IntStream.range(0, mParams.size()).mapToObj(i->{
                   return "lhp\n"+ //carico il valore di hp sullo stack
                           "sw\n" + //prende il valore che c è sullo stack (ind di hp) e ci mette il valore successivo, di cui fa una pop +
                           incrementHeapPointer();

               }).collect(Collectors.joining())+
               "push "+ (ExecuteVM.MEMSIZE+ mClassEntry.getOffset()) +"\n"+
               //metto sullo stack direttamente l'indirizzo di dove si trova il dispatch pointer(le classi partono dall'AR globale e di conseguenza nella codegen del class node noi mettiamo il dispatch pointer sullo stack)
                "lw\n"+
                "lhp\n"+
                "sw\n"+
                "lhp\n"+//lasciamo sullo stack il puntatore object pointer che punta al dispatch appena caricato
                incrementHeapPointer();

    }

    @Override
    public Type typeCheck() {
        Type type = mClassEntry.getType();
        if(type instanceof ClassType){
            final ClassType classType = (ClassType) type;
            if(classType.getAllFields().size()==mParams.size()){
                //controllo la varianza dei parametri della chiamata rispetto alla dichiarazione; posso passare boolean dove mi aspetto degli int;
                //typecheck lancia delle eccezioni in caso il typecheck abbia problemi di tipi quindi al return si giunge solo se tutto è ok
                boolean covarianza = IntStream.range(0, mParams.size()).allMatch(i->mParams.get(i).typeCheck().isSubtype(classType.getAllFields().get(i)));
                if(covarianza)
                    return RefType.of(mClassName);
                else
                    throw new RuntimeException("Wrong type of parameter for constructor invocation of class " + mClassName);
            }else{
                throw new RuntimeException("Wrong number of parameters for constructor of class " + mClassName);
            }
        }else{
            throw new RuntimeException("Invocation of constructor for a non class  "+ mClassName);
        }
    }

    @Override
    public String getTypeName() {
        return "NewNode";
    }

    @Override
    public String toPrint(String indent) {
        return indent + getTypeName() + "(" + mClassName + ") at level: " + mNestingLevel + "\n" +
                mClassEntry.toPrint(indent + DOUBLE_SPACE) +
                mParams.stream().map(p -> p.toPrint(indent + DOUBLE_SPACE)).collect(Collectors.joining());
    }

    private String incrementHeapPointer(){
        return "lhp\n"+
                "push 1\n"+
                "add\n"+
                "shp\n";//queste 4 righe incrementano hp
    }
}
