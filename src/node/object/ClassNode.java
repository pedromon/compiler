package node.object;

import node.interfaces.DeclarationNode;
import node.tables.DispatchTables;
import node.tables.STEntry;
import type.ClassType;
import type.Type;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.IntConsumer;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ClassNode implements DeclarationNode {

    private String mClassName;
    private Optional<String> mSuperClassName;
    private Optional<STEntry> mSuperClassEntry;
    private List<FieldNode> mFields;
    private List<MethodNode> mMethods;
    private ClassType mClassType;

    public ClassNode(String className, ClassType classType) {
        mClassName = className;
        mSuperClassName = Optional.empty();
        mSuperClassEntry = Optional.empty();
        mFields = new ArrayList<>();
        mMethods = new ArrayList<>();
        mClassType = classType;
    }

    public void addSuperClass(String superClassName, STEntry superClassEntry) {
        mSuperClassName = Optional.ofNullable(superClassName);
        mSuperClassEntry = Optional.ofNullable(superClassEntry);
    }

    public void addFieldNode(FieldNode fieldNode) {
        mFields.add(fieldNode);
    }

    public void addMethodNode(MethodNode methodNode) {
        mMethods.add(methodNode);
    }

    public String getClassName() {
        return mClassName;
    }

    public Optional<String> getSuperClassName() {
        return mSuperClassName;
    }

    @Override
    public Type getSymType() {
        return mClassType;
    }

    @Override
    public String codeGen() {
        //METODI
        List<String> dispatchTable = new ArrayList<>();
        if(mSuperClassEntry.isPresent()){
            dispatchTable.addAll(DispatchTables.getInstance().getDispatch(- (mSuperClassEntry.get().getOffset()) -2));
        }
        mMethods.stream().forEach(m->{
            m.codeGen();
            if(m.getOffset()<dispatchTable.size()){
                dispatchTable.set(m.getOffset(), m.getLabel());
            }else{
                dispatchTable.add(m.getLabel());
            }
            //se abbiamo ereditearietà il metodo sarà gia presente nella lista, e l'etichetta sarà riferita al metodo della superclasse; noi dobbiamo cambiarla e metterci la nuova etichetta
        });
        //abbiamo preparato la lista di stringhe contenenti le etichette dei metodi, ora la aggiungiamo
        DispatchTables.getInstance().addDispatchTable(dispatchTable);

        return "lhp \n" + //lascio il dispatch pointer sullo stack; ora andremo a muovere l heap durante la creazione della dispatch perchè ci inseriamo le etichette dei metodi
                dispatchTable.stream().map(l->{
                    return "push "+l+"\n"+
                    "lhp \n"+
                    "sw\n"+//carichiamo prima l etichetta che vogliamo memorizare nello heap, dopodichè salviamo l indirizzo di hp sullo stack
                    //sw fa prima una pop di hp e poi mette il valore recuperato con una pop successiva a quell indirizzo
                    "lhp \n"+
                    "push 1 \n"+
                    "add \n"+ //metto sullo stack l indirizzo di hp incrementato di 1
                    "shp\n";  //prendo il nuovo valore di hp che trovo sullo stack
                }).collect(Collectors.joining());


    }

    @Override
    public Type typeCheck() {
        mFields.stream().forEach(f->f.typeCheck());// lancia eccezione se i campi sono "overridati" male
        //nel caso in cui la classe estende un'altra classe superclassEntry non è vuoto
        if(mSuperClassEntry.isPresent()){
            if(mSuperClassEntry.get().getType() instanceof ClassType){
                ClassType superClassType = (ClassType) mSuperClassEntry.get().getType();
                ClassType reductBase = new ClassType();
                ClassType reductSuper = new ClassType();
                mFields.forEach(f->{
                    int position = -f.getOffset()-1;
                    if(position < superClassType.getFieldsCount()){
                        reductBase.addFieldType(position, mClassType.getAllFields().get(position));
                        reductSuper.addFieldType(position, superClassType.getAllFields().get(position));
                    }
                });

                mMethods.forEach(m->{
                    int position = m.getOffset();
                    if(position < superClassType.getMethodsCount()){
                        reductBase.addMethodType(position, mClassType.getAllMethods().get(position));
                        reductSuper.addMethodType(position, superClassType.getAllMethods().get(position));
                    }
                });
                reductBase.isSubtype(reductSuper);
            }
        }
        return getSymType();
    }

    @Override
    public String getTypeName() {
        return "ClassNode";
    }

    @Override
    public String toPrint(String indent) {
        return indent + getTypeName() + "(" + mClassName + mSuperClassName.map(s -> " ext " + s).orElse("") + ")\n" +
                mSuperClassEntry.map(e -> e.toPrint(indent + DOUBLE_SPACE)).orElse("") +
                mFields.stream().map(f -> f.toPrint(indent + DOUBLE_SPACE)).collect(Collectors.joining()) +
                mMethods.stream().map(m -> m.toPrint(indent + DOUBLE_SPACE)).collect(Collectors.joining());
    }
}
