package node.object;

import node.ParNode;
import type.Type;

public class FieldNode extends ParNode {

    private int offset;

    public FieldNode(String id, Type type) {
        super(id, type);
    }

    @Override
    public String getTypeName() {
        return "FieldNode";
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getOffset() {
        return offset;
    }
}
