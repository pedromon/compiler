package node.object;

import node.FunNode;
import node.interfaces.DeclarationNode;
import type.Type;
import utils.FunctionUtils;
import utils.LabelGenerator;

public class MethodNode extends FunNode {

    public String label;
    public int offset;

    public MethodNode(String id, Type retType) {
        super(id, retType);
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    @Override
    public String codeGen() {
        label = LabelGenerator.getInstance().generateMethodLabel();

        FunctionUtils.addFunctionCode(functionCodeGen(label));

        return "";

    }

    public int getOffset() {
        return offset;
    }

    public String getLabel(){
        return label;
    }

    @Override
    public String getTypeName() {
        return "MethodNode";
    }
}
