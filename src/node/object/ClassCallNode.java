package node.object;

import node.CallNode;
import node.interfaces.Node;
import node.tables.STEntry;
import type.Type;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ClassCallNode extends CallNode {

    private STEntry mObjectEntry;

    public ClassCallNode(String methodName, STEntry objectEntry, STEntry methodEntry, List<Node> params, int nestingLevel) {
        super(methodName, methodEntry, params, nestingLevel);
        mObjectEntry = objectEntry;
    }

    @Override
    public String codeGen() {
        return  "lfp\n"+ //setto il control link
                IntStream.range(0, getParams().size()).mapToObj(i->getParams().get(getParams().size()-1-i)).map(Node::codeGen).collect(Collectors.joining())+ //faccio push della codegen dei parametri IN ORDINE INVERSO!!!

                "push "+ mObjectEntry.getOffset() + "\n"+/*setto l'access link all'object pointer*/
                "lfp \n"+
                IntStream.range(0, getNestingLevel()- mObjectEntry.getNestingLevel()).mapToObj(i->"lw\n").collect(Collectors.joining())+
                "add\n"+
                "lw\n"+

                "push "+ mObjectEntry.getOffset() + "\n"+/*carico sullo stack l'object pointer*/
                "lfp \n"+
                IntStream.range(0, getNestingLevel()- mObjectEntry.getNestingLevel()).mapToObj(i->"lw\n").collect(Collectors.joining())+
                "add\n"+
                "lw\n"+

                "lw\n"+
                "push "+ getSTEntry().getOffset()+"\n"+//getSTEntry corrisponde alla methodentry;
                "add\n"+
                "lw\n"+//carico sullo stack l'indirizzo dove ho la codegen del metodo
                "js\n";

    }

    @Override
    public Type typeCheck() {
        //è uguale al CallNode perchè con il parsing io sono sicuro di avere nella STEntry dell'oggetto
        //un reftypenode sul campo tipo della entry
        return super.typeCheck();

    }

    @Override
    public String getTypeName() {
        return "ClassCallNode";
    }

    @Override
    public String toPrint(String indent) {
        return super.toPrint(indent) +
                mObjectEntry.toPrint(indent + DOUBLE_SPACE);
    }
}
