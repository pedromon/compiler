package node;

import node.abstractnode.SingleNode;
import node.interfaces.Node;
import type.Type;

public class PrintNode extends SingleNode<Node> {

    public PrintNode(Node value) {
        super(value);
    }

    @Override
    public String getTypeName() {
        return "Print";
    }

    @Override
    public String codeGen() {
        return getValue().codeGen()+"print\n";
    }

    @Override
    public Type typeCheck() {
        return value.typeCheck();
    }
}
