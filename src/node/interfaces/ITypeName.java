package node.interfaces;

public interface ITypeName {
    public String getTypeName();
}
