package node.interfaces;

import type.Type;

public interface DeclarationNode extends Node{
    public Type getSymType();
}
