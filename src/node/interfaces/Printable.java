package node.interfaces;

public interface Printable {

    static final String DOUBLE_SPACE = "  ";

    String toPrint(String indent);

}
