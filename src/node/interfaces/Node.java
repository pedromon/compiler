package node.interfaces;

public interface Node extends Printable, ITypeName, Checkable {
    public String codeGen();
}
