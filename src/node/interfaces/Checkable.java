package node.interfaces;

import type.Type;

public interface Checkable {

    public Type typeCheck();
}
