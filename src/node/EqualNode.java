package node;

import node.abstractnode.BinaryNode;
import node.interfaces.Node;
import type.ArrowType;
import type.BoolType;
import type.Type;
import utils.LabelGenerator;
import utils.LabelGenerator.Label;

public class EqualNode extends BinaryNode {

    public EqualNode(Node n1, Node n2) {
        super(n1, n2);
    }

    @Override
    public String getTypeName() {
        return "Equal";
    }

    @Override
    public String codeGen() {
        String label = LabelGenerator.getInstance().generateLabel(Label.GENERIC);
        String labelExit = LabelGenerator.getInstance().generateLabel(Label.EXITEQ);


        return left.codeGen()+
                right.codeGen()+
                "beq "+ label + "\n"+
                "push 0\n"+//se arrivo qui significa che non sono uguali e non ho saltato
                "b "+labelExit+ " \n"+
                label+":"+"\n"+
                "push 1\n"+
                labelExit+ ":\n";

        /* dovendo restituire un booleano deve lasciare in cima allo stack o 0 o 1 alla fine, percio metto la generazione delle due espressioni sullo stack
        e salto se sono uguali in un punto in cui faccio una push di 1(dico quindi che sono veri).
        se invece non sono uguali procedo nell esecuzione e faccio un push di 0, indicando che sono falsi.
        dopo aver detto che sono falsi, per non fare un  push sullo stack di 1, faccio un salt incondizionato
        */
    }


    @Override
    public Type typeCheck() {
        if(left.typeCheck() instanceof ArrowType || right.typeCheck() instanceof ArrowType){
            throw new RuntimeException("Arrow Type operando in equals");
        }
        if(left.typeCheck().isSubtype(right.typeCheck()) || right.typeCheck().isSubtype(left.typeCheck())){
            return BoolType.TYPE;
        }else{
            throw new RuntimeException("Incompatibles type in equals");
        }
    }
}
