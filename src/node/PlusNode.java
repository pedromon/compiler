package node;

import node.abstractnode.BinaryNode;
import node.interfaces.Node;
import type.IntType;
import type.Type;

public class PlusNode extends BinaryNode {

    //nodo1 e nodo2 sono a loro volta espressioni, che a loro volta possono essere fattori ecc.. (secondo la grammatica specificata nel file FOOL.g4
    public PlusNode(Node n1, Node n2) {
        super(n1, n2);
    }

    @Override
    public String getTypeName() {
        return "Plus";
    }

    @Override
    public String codeGen() {
        return left.codeGen()
                + right.codeGen()
                + "add\n";
    }

    @Override
    public Type typeCheck() {
        if(left.typeCheck().isSubtype(IntType.TYPE) && right.typeCheck().isSubtype(IntType.TYPE)){
            return IntType.TYPE;
        }else {
            throw new RuntimeException("Incompatible value for plus operator");
        }
    }
}
