package node;

import node.abstractnode.SingleNode;
import type.IntType;
import type.Type;

public class IntNode extends SingleNode<Integer> {

    public IntNode(Integer value) {
        super(value);
    }

    @Override
    public String getTypeName() {
        return "IntNode";
    }

    @Override
    public String codeGen() {
        return "push " + getValue() + "\n";
    }

    @Override
    public Type typeCheck() {
        return IntType.TYPE;
    }
}
