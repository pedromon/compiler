package node;

import node.abstractnode.SingleNode;
import type.BoolType;
import type.Type;

public class BoolNode extends SingleNode<Boolean> {

    public BoolNode(Boolean value) {
        super(value);
    }

    @Override
    public String getTypeName() {
        return "BoolNode";
    }

    @Override
    public String codeGen() {
        return "push " + (getValue() ? 1 : 0) + "\n";
    }

    @Override
    public Type typeCheck() {
        return BoolType.TYPE;
    }
}
