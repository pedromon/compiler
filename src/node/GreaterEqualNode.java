package node;

import node.abstractnode.BinaryNode;
import node.interfaces.Node;
import type.BoolType;
import type.Type;
import utils.LabelGenerator;

public class GreaterEqualNode extends BinaryNode {

    public GreaterEqualNode(Node n1, Node n2) {
        super(n1, n2);
    }

    @Override
    public String codeGen() {
        String label = LabelGenerator.getInstance().generateLabel(LabelGenerator.Label.GENERIC);
        String labelExit = LabelGenerator.getInstance().generateLabel(LabelGenerator.Label.EXITGEQ);

        return left.codeGen()+
                right.codeGen()+
                "sub\n"+
                "push -1\n"+
                "bleq "+ label + "\n"+
                "push 1\n"+//se arrivo qui significa che non sono minori uguali e non ho saltato
                "b "+labelExit+ " \n"+
                label+":"+"\n"+
                "push 0\n"+
                labelExit+ ":\n";

        /*l'idea  quella di sottrarre i due valori. se la loro differenza è < 0 il primo è piu grande del secondo e viceversa*/
    }

    @Override
    public String getTypeName() {
        return "GreaterEqualNode";
    }

    @Override
    public Type typeCheck() {
        if(left.typeCheck().isSubtype(right.typeCheck()) || right.typeCheck().isSubtype(left.typeCheck())){
            return BoolType.TYPE;
        }else{
            throw new RuntimeException("Incompatibles type in  greater equals");
        }
    }
}
