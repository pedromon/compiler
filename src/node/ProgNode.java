package node;

import node.abstractnode.SingleNode;
import node.interfaces.Node;
import type.Type;

//il programma può essere o una espressione lunga (5+6+7+8) -> ProgNode
// oppure un insieme di dichiarazioni e espressione -> ProgLetInNode

public class ProgNode extends SingleNode<Node> {

    public ProgNode(Node value) {
        super(value);
    }

    @Override
    public String codeGen() {
        return value.codeGen()+"halt\n";
    }

    @Override
    public String getTypeName() {
        return "Prog";
    }

    @Override
    public String toPrint(String indent) {
        return indent + getTypeName() + ":\n" + getValue().toPrint(indent+DOUBLE_SPACE);
    }

    @Override
    public Type typeCheck() {
        return value.typeCheck();
    }
}
