package node;

import node.interfaces.DeclarationNode;
import node.interfaces.Node;
import node.object.ClassNode;
import type.Type;
import utils.FunctionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ProgLetInNode implements Node {

    private List<ClassNode> mClassList;
    private List<DeclarationNode> mDecList;
    private Node exp;

    public ProgLetInNode(List<ClassNode> classList, List<DeclarationNode> decList, Node exp) {
        mClassList = classList;
        mDecList = decList;
        this.exp = exp;
    }

    @Override
    public String codeGen() {
        return "push 0\n"+ //return address fittizio
                mClassList.stream().map(Node::codeGen).collect(Collectors.joining()) +
                mDecList.stream().map(Node::codeGen).collect(Collectors.joining())+
                exp.codeGen()+
                "halt\n"+
                FunctionUtils.getFunctionCode();

    }

    @Override
    public String getTypeName() {
        return "LetIn";
    }

    @Override
    public String toPrint(String indent) {
        return indent + getTypeName() +":\n" +
                mClassList.stream().map(c -> c.toPrint(indent + DOUBLE_SPACE)).collect(Collectors.joining()) +
                mDecList.stream().map(dec-> dec.toPrint(indent + DOUBLE_SPACE)).collect(Collectors.joining()) +
                exp.toPrint(indent + DOUBLE_SPACE);
    }

    @Override
    public Type typeCheck() {
        mClassList.forEach(cl->cl.typeCheck());
        mDecList.forEach(dec->dec.typeCheck());
        return exp.typeCheck();
    }


}
