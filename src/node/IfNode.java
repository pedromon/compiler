package node;

import node.abstractnode.TrieNode;
import node.interfaces.Node;
import type.BoolType;
import type.Type;
import utils.InheritanceMap;
import utils.LabelGenerator;
import utils.LabelGenerator.Label;

import java.util.Optional;

public class IfNode extends TrieNode {

    public IfNode(Node thenBody, Node condition, Node elseBody) {
        super(thenBody, condition, elseBody);
    }

    @Override
    public String getTypeName() {
        return "IfNode";
    }

    @Override
    public Type typeCheck() {
        if(!getCenter().typeCheck().isSubtype( BoolType.TYPE)) {
            throw new RuntimeException("No boolean condition inside if.");
        }
        Type thenType = getLeft().typeCheck();
        Type elseType = getRight().typeCheck();
        Optional<Type> lowestCommonAncestor = InheritanceMap.getInstance().lowestCommonAncestor(thenType, elseType);
        if(lowestCommonAncestor.isPresent()){
            return lowestCommonAncestor.get();
        }else {
            throw new RuntimeException("Invalid if type");
        }
    }

    @Override
    public String codeGen() {
        String labelThen = LabelGenerator.getInstance().generateLabel(Label.THEN);
        String labelExit = LabelGenerator.getInstance().generateLabel(Label.EXITIF);

        return getCenter().codeGen()+
                "push 1\n"+
                "beq "+  labelThen + "\n"+
                getRight().codeGen()+
                "b "+labelExit+ "\n"+
                labelThen +":\n"
                + getLeft().codeGen()+
                labelExit+ ":\n";

        /*l'idea è quella di mettere un un uno (che significa true) in cima allo stack
            la codegen della condizione metterà un uno o zero a seconda che la condizione sia vera o falsa; dopodiche si salta al then se la condizione è vera
            se beq non salta eseguiamo il ramo else e dopo averlo terminato per non finire a eseguire il ramo then (che è esattamente sotto) saltiamo alla etichetta di uscita
         */
    }
}
