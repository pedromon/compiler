package node;

import node.abstractnode.SingleNode;
import node.interfaces.Node;
import node.interfaces.Printable;
import node.tables.STEntry;
import type.ArrowType;
import type.Type;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


/* Nodo per indicare chiamate di funzioni, e anche metodi (attenzione!! la chiamata di metodi ricade qui solo per metodi richiamati internamente dalle classi; la chiamata di metodi a partire dagli oggetti sarebbe un classCallNode
 * !!! Estende SingleNode con stringa, perchè contiene il nome della funzione */
public class CallNode extends SingleNode<String> {

    private STEntry mSTEntry;       /* STEntry relativa alla dichiarazione della funzione */
    private List<Node> mParams;     /* Elenco dei parametri passati alla funzione */
    private int mNestingLevel;      /* Nesting level dell'uso */

    public CallNode(String functionName, STEntry entry, List<Node> params, int nestingLevel) {
        super(functionName);
        mSTEntry = entry;
        mParams = params;
        mNestingLevel = nestingLevel;
    }

    @Override
    public String codeGen() {
        if(getSTEntry().isMethod()){
            return "lfp \n"+ //SETTO IL CONTROL LINK: io chiamante ti dico che il tuo control link punta a me (mio FP);
                    IntStream.range(0, mParams.size()).mapToObj(i->mParams.get(mParams.size()-1-i)).map(Node::codeGen).collect(Collectors.joining())+ //faccio push della codegen dei parametri IN ORDINE INVERSO!!!

                    //setta il valore dell access link, che punterà al frame pointer di chi ci contiene/dichiara
                    "lfp \n"+ //carico il valore del registro fp sullo stack (fp contiene l indirizzo di memoria in cui è contenuto l access  link); mi permette di effettuare la risalita, che una volta terminata mi ha lasciato sullo stack l indirizzo di memoria dell access link
                    IntStream.range(mSTEntry.getNestingLevel(), mNestingLevel).mapToObj(n->"lw\n").collect(Collectors.joining())+
                    //arrivati fino qui l'AR che il chiamante deve preparare è pronto

                    //IDENTICO ALL USO DI  VARIABILE
                    //pusho l offset
                    //risalgo la catena statica fino a raggiungere l access link(indirizzo)
                    "push "+mSTEntry.getOffset()+ "\n" +
                    "lfp \n"+
                    IntStream.range(mSTEntry.getNestingLevel(), mNestingLevel).mapToObj(n->"lw\n").collect(Collectors.joining())+
                    (mSTEntry.isMethod() ? "lw\n" : "" ) + //fa un salto aggiuntivo se è un metodo
                    "add \n"+
                    //con la somma tra offset e AR ho sulla cima dello  stack l indirizzo della cella che contiene l'indirizzo della funzione (la codegen di push etichetta lascia l indirizzo in cui trovo il codegen della funzione (dopo halt)
                    "lw \n"+
                    "js\n"; //salta al corpo della funzione
        }else{
            return "lfp \n"+ //SETTO IL CONTROL LINK: io chiamante ti dico che il tuo control link punta a me (mio FP);
                    IntStream.range(0, mParams.size()).mapToObj(i->mParams.get(mParams.size()-1-i)).map(Node::codeGen).collect(Collectors.joining())+ //faccio push della codegen dei parametri IN ORDINE INVERSO!!!

                    //setta il valore dell access link, che punterà al frame pointer di chi ci contiene/dichiara
                    "push "+mSTEntry.getOffset()+"\n"+ //AGGIUNTA HIGHER ORDER
                    "lfp \n"+ //carico il valore del registro fp sullo stack (fp contiene l indirizzo di memoria in cui è contenuto l access  link); mi permette di effettuare la risalita, che una volta terminata mi ha lasciato sullo stack l indirizzo di memoria dell access link
                    IntStream.range(mSTEntry.getNestingLevel(), mNestingLevel).mapToObj(n->"lw\n").collect(Collectors.joining())+
                    //arrivati fino qui l'AR che il chiamante deve preparare è pronto
                    "add \n"+//dopo aver fatto l add ci troviamo nella cella che contiene il frame pointer dell'AR che ci contiene (AGGIUNTA HIGHER ORDER)
                    "lw \n"+ //(AGGIUNTA HIGHER ORDER)

                    //IDENTICO ALL USO DI  VARIABILE
                    //pusho l offset
                    //risalgo la catena statica fino a raggiungere l access link(indirizzo)
                    "push "+ (mSTEntry.getOffset()-1)+ "\n" +//-1 AGGIUNTA HIGHER ORDER
                    "lfp \n"+
                    IntStream.range(mSTEntry.getNestingLevel(), mNestingLevel).mapToObj(n->"lw\n").collect(Collectors.joining())+
                    "add \n"+
                    //con la somma tra offset e AR ho sulla cima dello  stack l indirizzo della cella che contiene l'indirizzo della funzione (la codegen di push etichetta lascia l indirizzo in cui trovo il codegen della funzione (dopo halt)
                    "lw \n"+ //prende l indirizzo che è dentro l indirizzo (strano ma vero)
                    "js\n"; //salta al corpo della funzione

        }
    }

    @Override
    public String getTypeName() {
        return "CallNode";
    }

    protected int getNestingLevel() {
        return mNestingLevel;
    }

    protected List<Node> getParams() {
        return mParams;
    }

    protected STEntry getSTEntry(){
        return mSTEntry;
    }

    @Override
    public String toPrint(String indent) {
        return super.toPrint(indent) +
                indent + Printable.DOUBLE_SPACE + " at level: " + mNestingLevel + "\n" +
                mSTEntry.toPrint(indent + Printable.DOUBLE_SPACE) +
                mParams.stream().map(p -> p.toPrint(indent + DOUBLE_SPACE)).collect(Collectors.joining()) + "\n";
    }

    @Override
    public Type typeCheck() {
        Type type = mSTEntry.getType();
        if(type instanceof ArrowType){
            final ArrowType arrowType = (ArrowType) type;
            if(arrowType.getParList().size()==mParams.size()){
                //controllo la varianza dei parametri della chiamata rispetto alla dichiarazione; posso passare boolean dove mi aspetto degli int;
                //typecheck lancia delle eccezioni in caso il typecheck abbia problemi di tipi quindi al return si giunge solo se tutto è ok
                boolean covarianza = IntStream.range(0, mParams.size()).allMatch(i->mParams.get(i).typeCheck().isSubtype(arrowType.getParList().get(i)));
                if(covarianza)
                    return arrowType.getRetType();
                else
                    throw new RuntimeException("Wrong type of parameter for function invocation " + getValue());
            }else{
                throw new RuntimeException("Wrong number of parameters for function " + getValue());
            }
        }else{
            throw new RuntimeException("Invocation of a non function "+getValue());
        }
    }


}
