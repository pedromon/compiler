package node;

import node.interfaces.DeclarationNode;
import node.interfaces.Node;
import type.Type;

public class ParNode implements DeclarationNode {

    private String id;
    private Type type;

    public ParNode(String id, Type type) {
        this.id = id;
        this.type = type;
    }

    @Override
    public String codeGen() {
        return null;
    }

    @Override
    public String toPrint(String indent) {
        return indent +  getTypeName()+ ":" + id + "\n"
                + type.toPrint(indent + DOUBLE_SPACE);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    @Override
    public String getTypeName() {
        return "ParNode";
    }

    @Override
    public Type typeCheck() {
        return type;
    }

    @Override
    public Type getSymType() {
        return type;
    }
}
