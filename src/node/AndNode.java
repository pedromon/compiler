package node;

import node.abstractnode.BinaryNode;
import node.interfaces.Node;
import type.BoolType;
import type.Type;
import utils.LabelGenerator;

public class AndNode extends BinaryNode {

    public AndNode(Node n1, Node n2) {
        super(n1, n2);
    }

    @Override
    public String codeGen() {
        String label = LabelGenerator.getInstance().generateLabel(LabelGenerator.Label.GENERIC);
        String labelFalse = LabelGenerator.getInstance().generateLabel(LabelGenerator.Label.GENERIC);

        return left.codeGen()+
                "push 0\n"+
                "beq " + labelFalse+ "\n"+
                right.codeGen()+
                "push 0\n"+
                "beq "+labelFalse +"\n"+
                "push 1 \n"+
                "b " + label+"\n"+
                labelFalse +":\n"+
                "push 0\n"+
                label+":"+"\n";

        /* valutiamo la codegen della prima espressione; se questa è falsa (quindi = 0) saltiamo in un punto in cui pusheremo 0 in quanto l espressione è tutta falsa;
            se questa è vera continuamo l esecuzione controllando che anche l altra sia non vera;
            se non è vera salto a labelfalse, dove andiamo a pushare uno zero in quanto tutta l espressione è valutata falsa
           . se invece sono entrambe vere pushamo un 1 e per non andare a pushare uno 0 saltiamo incondizionatamente alla fine
         */
    }

    @Override
    public String getTypeName() {
        return "AndNode";
    }

    @Override
    public Type typeCheck() {
        if(left.typeCheck().equals(BoolType.TYPE) && right.typeCheck().equals(BoolType.TYPE)){
            return BoolType.TYPE;
        }else{
            throw new RuntimeException("Incompatible value for and operator");
        }
    }
}
