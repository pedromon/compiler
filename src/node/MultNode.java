package node;

import node.abstractnode.BinaryNode;
import node.interfaces.Node;
import type.IntType;
import type.Type;

public class MultNode extends BinaryNode {

    public MultNode(Node n1, Node n2) {
        super(n1, n2);
    }

    @Override
    public String getTypeName()  {
        return "Mult";
    }

    @Override
    public String codeGen() {
        return left.codeGen()
                + right.codeGen()
                + "mult\n";
    }

    @Override
    public Type typeCheck() {
        if(left.typeCheck().isSubtype(IntType.TYPE) && right.typeCheck().isSubtype(IntType.TYPE)){
            return IntType.TYPE;
        }else{
            throw new RuntimeException("Incompatibles value in mult");
        }
    }
}
