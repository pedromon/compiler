package node.tables;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class VirtualTable {

    private Map<String, STEntry> mVirtualTable;

    public VirtualTable() {
        mVirtualTable = new HashMap<>();
    }

    public VirtualTable(VirtualTable toCopy) {
        mVirtualTable = new HashMap<>(toCopy.mVirtualTable);
    }

    public boolean addEntry(String nameMethodOrField, STEntry stEntry){
        // andiamo a recuperare, se esiste l'st entry del metodo o del campo che vogliamo aggiungere; se esiste già putifabsent
        //resituisce il suo valore; una volta ottenuto il valore andiamo a cambiargli l offset per permettere l override)
        STEntry duplicate = mVirtualTable.putIfAbsent(nameMethodOrField, stEntry);
        if(duplicate != null){
            /*
            se nome di campo/metodo è già presente, non lo considero
            errore, ma overriding: sostituisco nuova STentry alla vecchia
            preservando l’offset che era nella vecchia STentry
             */
            // controllo che override sia solo tra metodi o tra attributi
            if((duplicate.isMethod() && stEntry.isMethod()) || (!duplicate.isMethod() && !stEntry.isMethod()))
            {
                stEntry.setOffset(duplicate.getOffset());
                mVirtualTable.replace(nameMethodOrField, stEntry);
            } else {
                // sono nel caso in cui uno è metodo e uno è campo o viceversa. NON è POSSIBILE PERCHè CAMPI E METODI DI UNA CLASSE SONO ALLO STESSO NESTING LEVEL
                //PER DEFINIZIONE DI SYMBOL TABLE DUE ID ALLO STESSO NESTING LEVEL è COME SE FOSSE VAR GIA DICHIARATA
                return false;
            }
        }
        return true;
    }

    public Optional<STEntry> getSTEntry(String methodOrFieldName) {
        return Optional.ofNullable(mVirtualTable.get(methodOrFieldName));
    }

    public Map<String, STEntry> getReferenceToVirtualTable() {
        return mVirtualTable;
    }
}
