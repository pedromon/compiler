package node.tables;

import java.util.ArrayList;
import java.util.List;

public class DispatchTables {

    private static final DispatchTables INSTANCE = new DispatchTables();

    private static List<List<String>> dispatchTables;

    private DispatchTables(){
        dispatchTables = new ArrayList<>(new ArrayList<>());
    }

    public static DispatchTables getInstance(){
        return INSTANCE;
    }

    public void addDispatchTable(List<String> dispatchTable){
        dispatchTables.add(dispatchTable);
    }

    //l'indice passato equivale all'offset della classe +2 (perchè l'offset delle classi nella loro dichiarazione parte da -2)
    public List<String> getDispatch(int classPosition){
        return new ArrayList<>(dispatchTables.get(classPosition));
    }




}
