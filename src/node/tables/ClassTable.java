package node.tables;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class ClassTable {

    private Map<String,VirtualTable> mClassTable;

    public ClassTable() {
        mClassTable = new HashMap<>();
    }

    public VirtualTable initVirtualTable(String baseClassName, Optional<String> superClassName) {
        if(superClassName.isPresent()) {
            mClassTable.put(baseClassName, new VirtualTable(mClassTable.get(superClassName.get())));
        } else {
            mClassTable.put(baseClassName, new VirtualTable());
        }
        return mClassTable.get(baseClassName);
    }

    public boolean existClass(String className) {
        return mClassTable.containsKey(className);
    }

    public Optional<STEntry> getSTEntry(String className, String nameMethodOrField){
        //ci restituisce la STEntry di un metodo o di un campo
        return mClassTable.get(className).getSTEntry(nameMethodOrField);
    }

    public VirtualTable getVirtualTable(String className) {
        return mClassTable.get(className);
    }

    public int getNumberOfClassDeclaration() {
        return mClassTable.size();
    }
}
