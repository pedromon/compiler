package node.tables;

import javax.swing.text.html.Option;
import java.util.*;

public class SymbolTable {

    /* Lista di tabelle, dove ogni elemento della lista corrisponde ad un nesting level
     * mentre le singole mappe rappresentano le dichiarazioni incontrare in quel nesting level.
     * Ogni elemento della mappa a sua volta ha come chiave il nome della dichiarazione e relativa STEntry */
    private List<Map<String, STEntry>> mTables;

    /* Indica l'attuale nesting level usato dal parsing (sarebbe la variabile globale) */
    private int mCurrentNestingLevel;

    public SymbolTable() {
        mTables = new ArrayList<>();
        mCurrentNestingLevel = -1; /* Inizializzato a -1, nel momento dell'ingresso allo scope globale, viene portato a 0 */
    }

    /* 1. Incrementa nesting level,
     * 2. Aggiunge una nuova hashtable vuota */
    public void enterNestingLevel() {
        mCurrentNestingLevel++;
        mTables.add(new HashMap<>());
    }

    /* Elimino hastable del nesting level attuale e decremento il nesting level */
    public void exitNestingLevel() {
        mTables.remove(mCurrentNestingLevel--);
    }

    public int getCurrentNestingLevel() {
        return mCurrentNestingLevel;
    }


    //restituisce vero se ha inserito la entry nella mappa, falso se esiste gia
    public boolean addEntry(String name, STEntry stEntry){
        return mTables.get(mCurrentNestingLevel).putIfAbsent(name, stEntry) == null;
    }

    // imposta il riferimento della symbol table, presente al nesting level corrente (1), alla virtual table passata
    public boolean setReferenceToVirtualTable(VirtualTable virtualTable) {
        mTables.set(mCurrentNestingLevel, virtualTable.getReferenceToVirtualTable());
        return true;
    }

    public Optional<STEntry> findDeclaration(String decName){
        for(int i = mCurrentNestingLevel; i>=0;i--){
            if(mTables.get(i).containsKey(decName)){
                return Optional.of(mTables.get(i).get(decName));
            }
        }
        return Optional.empty();
    }

    public Optional<STEntry> findClassDeclaration(String className) {
        return Optional.ofNullable(mTables.get(0).get(className));
    }

}
