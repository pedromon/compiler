package node.tables;

import node.interfaces.Printable;
import type.Type;

public class STEntry implements Printable {

    private Type mType;
    private int mNestingLevel;
    private int mOffset;
    private boolean isMethod;

    public static STEntry makeFunctionSTEntry(Type type, int nestingLevel, int offset) {
        return new STEntry(type, nestingLevel, offset, false);
    }

    public static STEntry makeMethodSTEntry(Type type, int nestingLevel, int offset) {
        return new STEntry(type, nestingLevel, offset, true);
    }

    protected STEntry(Type type, int nestingLevel, int offset, boolean isMethod) {
        mType = type;
        mNestingLevel = nestingLevel;
        mOffset = offset;
        this.isMethod = isMethod;
    }

    public STEntry(Type type, int nestingLevel, int offset) {
        this(type, nestingLevel, offset, false);
    }

    @Override
    public String toPrint(String indent) {
        return indent + "STEntry -> nesting level: " + mNestingLevel + "\n" +
                indent + "STEntry -> type: \n" + mType.toPrint(indent + Printable.DOUBLE_SPACE) +
                indent + "STEntry -> offset: " + mOffset + "\n";

    }

    public Type getType() {
        return mType;
    }

    public int getNestingLevel() {
        return mNestingLevel;
    }

    public int getOffset() {
        return mOffset;
    }

    public void addType(Type type){
        this.mType = type;
    }

    public void setOffset(int offset) {
        mOffset = offset;
    }

    public boolean isMethod() {
        return isMethod;
    }
}
