package utils;

import type.*;

import java.net.Inet4Address;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.IntStream;

public class InheritanceMap {

    private static final InheritanceMap INSTANCE = new InheritanceMap();

    private InheritanceMap(){};

    private Map<String, Optional<String>> supertypeMap = new HashMap<>();

    public static InheritanceMap getInstance(){
        return INSTANCE;
    }

    public boolean isSupertype(String class1, String class2){
        if (class1.equals(class2)){
            //caso base 1: le classi sono le stesse
            return true;
        } else{
            if(!supertypeMap.get(class2).isPresent()){
                //caso base 2: ho risalito tutta la catena delle ereditarietà per la classe baseClass e non ho mai incontrato la class1
                return false;
            }else{
                return isSupertype(class1, supertypeMap.get(class2).get());
            }
        }
    }

    public void addInheritanceRel(String baseClass, Optional<String> superClass){
        supertypeMap.put(baseClass, superClass);
    }

    public Optional<Type> lowestCommonAncestor(Type a, Type b){
         if(a instanceof EmptyType){
             return Optional.of(b);
         }else if(b instanceof EmptyType){
             return Optional.of(a);
         }else if(a instanceof ArrowType && b instanceof ArrowType){
             if(((ArrowType) a).getParList().size() == ((ArrowType) b).getParList().size()){
                 Optional<Type> lowestRetType = lowestCommonAncestor(((ArrowType) a).getRetType(), ((ArrowType) b).getRetType());
                 if(lowestRetType.isPresent()){
                     ArrowType lowestCommonArrow = new ArrowType(lowestRetType.get());
                     List<Type> aParamTypes = ((ArrowType) a).getParList();
                     List<Type> bParamTypes = ((ArrowType) b).getParList();

                     boolean allSubtype = IntStream.range(0, aParamTypes.size()).mapToObj(index->{
                        if(aParamTypes.get(index).isSubtype(bParamTypes.get(index))){
                            lowestCommonArrow.addParToList(aParamTypes.get(index));
                            return true;
                        }else if(bParamTypes.get(index).isSubtype(aParamTypes.get(index))){
                            //faccio questo controllo pechè non è detta che se l'i-esimo di a non è sottotipo dell'i-esimo di b valga anche il contrario
                            lowestCommonArrow.addParToList(bParamTypes.get(index));
                            return true;
                        }
                        return false;
                     }).allMatch(t -> t);
                     return allSubtype ? Optional.of(lowestCommonArrow) : Optional.empty();
                 }

             }
         }else {
             if(a instanceof RefType && b instanceof RefType){
                Optional<String> ancestorClassName =  lowestAncestorClassName(((RefType)a).getClassName(), ((RefType)b).getClassName());
                if(ancestorClassName.isPresent()){
                    return Optional.of(RefType.of(ancestorClassName.get()));
                }
             }else if (a instanceof IntType || b instanceof IntType){
                 return Optional.of(IntType.TYPE);
             }else{
                 return Optional.of(BoolType.TYPE);
             }
         }
         return Optional.empty();
    }

    private Optional<String> lowestAncestorClassName(String a, String b){
        if(a.equals(b)){
            return Optional.of(a);
        }
        else if(!supertypeMap.containsKey(a) || !supertypeMap.get(a).isPresent()){
            return Optional.empty();
        }else{
            if(isSupertype(supertypeMap.get(a).get(),b )){
                return supertypeMap.get(a);
            }else{
                return lowestAncestorClassName(supertypeMap.get(a).get(), b);
            }
        }
    }

}
