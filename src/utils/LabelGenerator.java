package utils;


import java.util.HashMap;
import java.util.Map;

public class LabelGenerator {

    private static final LabelGenerator instance = getInstance();

    private Map<String, Integer> mCounters = new HashMap<>();

    public String generateLabel(Label label) {
        return generateLabel(label.toString());
    }

    public String generateLabel(String prefix) {
        mCounters.putIfAbsent(prefix, 0);

        int currentCounter = mCounters.get(prefix);
        mCounters.replace(prefix, ++currentCounter);
        return prefix + currentCounter;
    }

    public String generateFunctionLabel(){
        return generateLabel(Label.FUNCTION);
    }

    public String generateMethodLabel() {
        return generateLabel(Label.METHOD);
    }


    public static LabelGenerator getInstance(){
        return (instance == null) ? new LabelGenerator() : instance;
    }




    public enum Label{
        THEN("then"),
        EXITIF("exitif"),
        EXITEQ("exitequal"),
        EXITLEQ("exitlessequal"),
        EXITGEQ("exitgreaterequal"),

        GENERIC("label"),
        FUNCTION("fun"),
        METHOD("method");

        private String mString;

        Label(String string) {
            mString = string;
        }

        @Override
        public String toString() {
            return mString;
        }
    }
}
