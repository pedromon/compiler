package utils;

public class FunctionUtils {

    private static String FUNCTION_CODE = "";

    public static final String getFunctionCode(){
        return FUNCTION_CODE;
    }

    public static void addFunctionCode(String code){
        FUNCTION_CODE += "\n"+code;
    }
}
