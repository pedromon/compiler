package test;

import node.interfaces.Node;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import svm.ExecuteVM;
import svm.SVMLexer;
import svm.SVMParser;
import type.Type;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class Test {

    public static void main(String [] args) throws IOException {
        String fileName = "C:\\Users\\Andrea Petreti\\Documents\\IntelliJProject\\compiler\\src\\test\\prova.fool";

        CharStream chars = CharStreams.fromFileName(fileName);
        FOOLLexer lexer = new FOOLLexer(chars);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        FOOLParser parser = new FOOLParser(tokens);

        Node ast = parser.prog().ast;

        if(lexer.lexicalErrors == 0 && parser.getNumberOfSyntaxErrors() == 0) {

            System.out.println("Visualizing AST...");
            System.out.print(ast.toPrint(""));

            Type type = ast.typeCheck();
            System.out.println(type.toPrint("Type check Ok! "));

            // Code Generation
            String code = ast.codeGen();
            BufferedWriter out = new BufferedWriter(new FileWriter(fileName + ".asm"));
            out.write(code);
            out.close();
            System.out.println("Code Generated!");

            // Execution
            CharStream charsASM = CharStreams.fromFileName(fileName + ".asm");
            SVMLexer lexerASM = new SVMLexer(charsASM);
            CommonTokenStream tokensASM = new CommonTokenStream(lexerASM);
            SVMParser parserASM = new SVMParser(tokensASM);

            parserASM.assembly();

            System.out.println("You had: " + lexerASM.lexicalErrors + " lexical errors and "
                    + parserASM.getNumberOfSyntaxErrors() + " syntax errors.");
            if (lexerASM.lexicalErrors > 0 || parserASM.getNumberOfSyntaxErrors() > 0) System.exit(1);

            System.out.println("Starting Virtual Machine...");
            ExecuteVM vm = new ExecuteVM(parserASM.code);
            vm.cpu();
        } else {
            System.out.println("You had: " + lexer.lexicalErrors + " lexical errors and " + parser.getNumberOfSyntaxErrors() + " syntax errors.");
        }
    }
}
