// Generated from /Users/mone/eclipse-workspace/Compiler/FOOL.g4 by ANTLR 4.7.2
package test;

 import node.*;
 import node.interfaces.*;
 import node.abstractnode.*;
 import node.tables.*;
 import type.*;
 import java.util.*;
 import node.object.*;
 import utils.*;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class FOOLParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7.2", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		PLUS=1, MINUS=2, TIMES=3, DIV=4, LPAR=5, RPAR=6, CLPAR=7, CRPAR=8, SEMIC=9, 
		COLON=10, COMMA=11, DOT=12, OR=13, AND=14, NOT=15, GE=16, LE=17, EQ=18, 
		ASS=19, TRUE=20, FALSE=21, IF=22, THEN=23, ELSE=24, PRINT=25, LET=26, 
		IN=27, VAR=28, FUN=29, CLASS=30, EXTENDS=31, NEW=32, NULL=33, INT=34, 
		BOOL=35, ARROW=36, INTEGER=37, ID=38, WHITESP=39, COMMENT=40, ERR=41;
	public static final int
		RULE_prog = 0, RULE_cllist = 1, RULE_declist = 2, RULE_exp = 3, RULE_term = 4, 
		RULE_factor = 5, RULE_value = 6, RULE_hotype = 7, RULE_type = 8, RULE_arrow = 9;
	private static String[] makeRuleNames() {
		return new String[] {
			"prog", "cllist", "declist", "exp", "term", "factor", "value", "hotype", 
			"type", "arrow"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'+'", "'-'", "'*'", "'/'", "'('", "')'", "'{'", "'}'", "';'", 
			"':'", "','", "'.'", "'||'", "'&&'", "'!'", "'>='", "'<='", "'=='", "'='", 
			"'true'", "'false'", "'if'", "'then'", "'else'", "'print'", "'let'", 
			"'in'", "'var'", "'fun'", "'class'", "'extends'", "'new'", "'null'", 
			"'int'", "'bool'", "'->'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "PLUS", "MINUS", "TIMES", "DIV", "LPAR", "RPAR", "CLPAR", "CRPAR", 
			"SEMIC", "COLON", "COMMA", "DOT", "OR", "AND", "NOT", "GE", "LE", "EQ", 
			"ASS", "TRUE", "FALSE", "IF", "THEN", "ELSE", "PRINT", "LET", "IN", "VAR", 
			"FUN", "CLASS", "EXTENDS", "NEW", "NULL", "INT", "BOOL", "ARROW", "INTEGER", 
			"ID", "WHITESP", "COMMENT", "ERR"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "FOOL.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }


	private SymbolTable symTable = new SymbolTable();
	private ClassTable classTable = new ClassTable();

	public FOOLParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class ProgContext extends ParserRuleContext {
		public Node ast;
		public CllistContext cllist;
		public DeclistContext declist;
		public ExpContext exp;
		public TerminalNode SEMIC() { return getToken(FOOLParser.SEMIC, 0); }
		public TerminalNode LET() { return getToken(FOOLParser.LET, 0); }
		public TerminalNode IN() { return getToken(FOOLParser.IN, 0); }
		public ExpContext exp() {
			return getRuleContext(ExpContext.class,0);
		}
		public CllistContext cllist() {
			return getRuleContext(CllistContext.class,0);
		}
		public DeclistContext declist() {
			return getRuleContext(DeclistContext.class,0);
		}
		public ProgContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_prog; }
	}

	public final ProgContext prog() throws RecognitionException {
		ProgContext _localctx = new ProgContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_prog);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			 symTable.enterNestingLevel(); 
			setState(42);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case LET:
				{
				setState(21);
				match(LET);

				                int offset = -2;
				                List<ClassNode> classList = new ArrayList<>();
				                List<Node> decList = new ArrayList<>();
				            
				setState(33);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case CLASS:
					{
					setState(23);
					((ProgContext)_localctx).cllist = cllist();
					classList = ((ProgContext)_localctx).cllist.classList;
					setState(28);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==VAR || _la==FUN) {
						{
						setState(25);
						((ProgContext)_localctx).declist = declist();
						decList = ((ProgContext)_localctx).declist.decList;
						}
					}

					}
					break;
				case VAR:
				case FUN:
					{
					setState(30);
					((ProgContext)_localctx).declist = declist();
					decList = ((ProgContext)_localctx).declist.decList;
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(35);
				match(IN);
				setState(36);
				((ProgContext)_localctx).exp = exp();
				((ProgContext)_localctx).ast =  new ProgLetInNode(classList, decList, ((ProgContext)_localctx).exp.ast); 
				}
				break;
			case LPAR:
			case NOT:
			case TRUE:
			case FALSE:
			case IF:
			case PRINT:
			case NEW:
			case NULL:
			case INTEGER:
			case ID:
				{
				setState(39);
				((ProgContext)_localctx).exp = exp();
				 ((ProgContext)_localctx).ast = new ProgNode(((ProgContext)_localctx).exp.ast); 
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(44);
			match(SEMIC);
			 symTable.exitNestingLevel(); 
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CllistContext extends ParserRuleContext {
		public List<ClassNode> classList;
		public Token baseClassName;
		public Token superClassName;
		public Token fieldName;
		public TypeContext type;
		public Token methodName;
		public TypeContext retType;
		public Token paramName;
		public HotypeContext parType;
		public Token varName;
		public ExpContext exp;
		public List<TerminalNode> CLASS() { return getTokens(FOOLParser.CLASS); }
		public TerminalNode CLASS(int i) {
			return getToken(FOOLParser.CLASS, i);
		}
		public List<TerminalNode> LPAR() { return getTokens(FOOLParser.LPAR); }
		public TerminalNode LPAR(int i) {
			return getToken(FOOLParser.LPAR, i);
		}
		public List<TerminalNode> RPAR() { return getTokens(FOOLParser.RPAR); }
		public TerminalNode RPAR(int i) {
			return getToken(FOOLParser.RPAR, i);
		}
		public List<TerminalNode> CLPAR() { return getTokens(FOOLParser.CLPAR); }
		public TerminalNode CLPAR(int i) {
			return getToken(FOOLParser.CLPAR, i);
		}
		public List<TerminalNode> CRPAR() { return getTokens(FOOLParser.CRPAR); }
		public TerminalNode CRPAR(int i) {
			return getToken(FOOLParser.CRPAR, i);
		}
		public List<TerminalNode> ID() { return getTokens(FOOLParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(FOOLParser.ID, i);
		}
		public List<TerminalNode> EXTENDS() { return getTokens(FOOLParser.EXTENDS); }
		public TerminalNode EXTENDS(int i) {
			return getToken(FOOLParser.EXTENDS, i);
		}
		public List<TerminalNode> COLON() { return getTokens(FOOLParser.COLON); }
		public TerminalNode COLON(int i) {
			return getToken(FOOLParser.COLON, i);
		}
		public List<TypeContext> type() {
			return getRuleContexts(TypeContext.class);
		}
		public TypeContext type(int i) {
			return getRuleContext(TypeContext.class,i);
		}
		public List<TerminalNode> FUN() { return getTokens(FOOLParser.FUN); }
		public TerminalNode FUN(int i) {
			return getToken(FOOLParser.FUN, i);
		}
		public List<ExpContext> exp() {
			return getRuleContexts(ExpContext.class);
		}
		public ExpContext exp(int i) {
			return getRuleContext(ExpContext.class,i);
		}
		public List<TerminalNode> SEMIC() { return getTokens(FOOLParser.SEMIC); }
		public TerminalNode SEMIC(int i) {
			return getToken(FOOLParser.SEMIC, i);
		}
		public List<TerminalNode> COMMA() { return getTokens(FOOLParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(FOOLParser.COMMA, i);
		}
		public List<TerminalNode> LET() { return getTokens(FOOLParser.LET); }
		public TerminalNode LET(int i) {
			return getToken(FOOLParser.LET, i);
		}
		public List<TerminalNode> IN() { return getTokens(FOOLParser.IN); }
		public TerminalNode IN(int i) {
			return getToken(FOOLParser.IN, i);
		}
		public List<HotypeContext> hotype() {
			return getRuleContexts(HotypeContext.class);
		}
		public HotypeContext hotype(int i) {
			return getRuleContext(HotypeContext.class,i);
		}
		public List<TerminalNode> VAR() { return getTokens(FOOLParser.VAR); }
		public TerminalNode VAR(int i) {
			return getToken(FOOLParser.VAR, i);
		}
		public List<TerminalNode> ASS() { return getTokens(FOOLParser.ASS); }
		public TerminalNode ASS(int i) {
			return getToken(FOOLParser.ASS, i);
		}
		public CllistContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_cllist; }
	}

	public final CllistContext cllist() throws RecognitionException {
		CllistContext _localctx = new CllistContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_cllist);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			 int offset = -2; ((CllistContext)_localctx).classList =  new ArrayList<>(); 
			setState(134); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(48);
				match(CLASS);
				setState(49);
				((CllistContext)_localctx).baseClassName = match(ID);

				                Optional<String> superclassName = Optional.empty();
				                if(symTable.findClassDeclaration((((CllistContext)_localctx).baseClassName!=null?((CllistContext)_localctx).baseClassName.getText():null)).isPresent()) {
				                    throw new RuntimeException("Multiple class declaration " + (((CllistContext)_localctx).baseClassName!=null?((CllistContext)_localctx).baseClassName.getText():null) + " at line " + (((CllistContext)_localctx).baseClassName!=null?((CllistContext)_localctx).baseClassName.getLine():0));
				                }
				            
				setState(54);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==EXTENDS) {
					{
					setState(51);
					match(EXTENDS);
					setState(52);
					((CllistContext)_localctx).superClassName = match(ID);

					                superclassName = Optional.of((((CllistContext)_localctx).superClassName!=null?((CllistContext)_localctx).superClassName.getText():null));
					            
					}
				}


				                InheritanceMap.getInstance().addInheritanceRel((((CllistContext)_localctx).baseClassName!=null?((CllistContext)_localctx).baseClassName.getText():null), superclassName);
				                // se non si eredita creo un ClassType con le liste vuote
				                ClassType classType = new ClassType();
				                Optional<STEntry> superClassEntry = Optional.empty(); //non è detto che se superclassname non è empty anche questa è piena; potrei aver esteso da una classe non esistente
				                if(superclassName.isPresent()){
				                    superClassEntry = symTable.findClassDeclaration((((CllistContext)_localctx).superClassName!=null?((CllistContext)_localctx).superClassName.getText():null));
				                    if(!superClassEntry.isPresent()) {
				                        throw new RuntimeException("Extend to non existing class " + (((CllistContext)_localctx).superClassName!=null?((CllistContext)_localctx).superClassName.getText():null) + " at line " + (((CllistContext)_localctx).superClassName!=null?((CllistContext)_localctx).superClassName.getLine():0));
				                    } else {
				                        // recupero il class type della classe padre (super)
				                        // sono certo che il downcast è corretto, perchè sono sicuro che la STEntry è di una classe e che quindi
				                        // il tipo è ClassType, perchè possiamo avere solo dichiarazioni di classi prima di variabili o funzioni
				                        ClassType superClassType = (ClassType) superClassEntry.get().getType(); // prendo la ClassType della classe padre, cioè da cui estendo.
				                        classType = new ClassType(superClassType); // costruisco la mia ClassType a partire dalla ClassType della super classe.
				                    }
				                }

				                ClassNode classNode = new ClassNode((((CllistContext)_localctx).baseClassName!=null?((CllistContext)_localctx).baseClassName.getText():null), classType);
				                if(superClassEntry.isPresent() && superclassName.isPresent()) {
				                    classNode.addSuperClass(superclassName.get(), superClassEntry.get());
				                }
				                _localctx.classList.add(classNode);

				                 // aggiungo dichiarazione della classe in symbol table, non controllo se ci sono duplicati, perchè l'ho
				                 // già fatto in precendenza con class table, quindi se c'era una classe con stesso nome, avrei già lanciato l'eccezione.
				                 STEntry classEntry = new STEntry(classType, symTable.getCurrentNestingLevel(), offset--);
				                 symTable.addEntry((((CllistContext)_localctx).baseClassName!=null?((CllistContext)_localctx).baseClassName.getText():null), classEntry);
				                 VirtualTable virtualTable = classTable.initVirtualTable((((CllistContext)_localctx).baseClassName!=null?((CllistContext)_localctx).baseClassName.getText():null), Optional.ofNullable((((CllistContext)_localctx).superClassName!=null?((CllistContext)_localctx).superClassName.getText():null)));

				                 // entro in nesting level 1
				                 symTable.enterNestingLevel();
				                 symTable.setReferenceToVirtualTable(virtualTable);
				            
				setState(57);
				match(LPAR);
				setState(73);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==ID) {
					{
					setState(58);
					((CllistContext)_localctx).fieldName = match(ID);
					setState(59);
					match(COLON);
					setState(60);
					((CllistContext)_localctx).type = type();

					                        // Offset dei campi viene inizializzato a -1 se non si eredita, altrimenti lo setto in base alla
					                        // al numero di campi della classe da cui estendo, ovvero "-lunghezza-1"
					                        int offsetField = -1;
					                        if(superClassEntry.isPresent()) {
					                            ClassType superClassType = ((ClassType) superClassEntry.get().getType());
					                            offsetField = -(superClassType.getFieldsCount()) - 1;
					                        }

					                        // aggiungo nodo del campo al nodo della classe.
					                        classNode.addFieldNode(new FieldNode((((CllistContext)_localctx).fieldName!=null?((CllistContext)_localctx).fieldName.getText():null), ((CllistContext)_localctx).type.mType));
					                        STEntry fieldEntry = new STEntry(((CllistContext)_localctx).type.mType, symTable.getCurrentNestingLevel(), offsetField--);
					                        virtualTable.addEntry((((CllistContext)_localctx).fieldName!=null?((CllistContext)_localctx).fieldName.getText():null), fieldEntry);

					                        // per mantenere coerenza con la virtual table in caso di override, il vecchio tipo del campo
					                        // viene sostituito dal nuovo tipo del campo dichiarato per l'override, infatti viene rimpiazzato
					                        // in posizione -offset-1. Dove l'offset è quello inserito nella STEntry del nuovo campo.
					                        classType.addFieldType(-fieldEntry.getOffset() - 1, fieldEntry.getType());
					                    
					setState(70);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==COMMA) {
						{
						{
						setState(62);
						match(COMMA);
						setState(63);
						((CllistContext)_localctx).fieldName = match(ID);
						setState(64);
						match(COLON);
						setState(65);
						((CllistContext)_localctx).type = type();

						                    classNode.addFieldNode(new FieldNode((((CllistContext)_localctx).fieldName!=null?((CllistContext)_localctx).fieldName.getText():null), ((CllistContext)_localctx).type.mType));
						                    fieldEntry = new STEntry(((CllistContext)_localctx).type.mType, symTable.getCurrentNestingLevel(), offsetField--);
						                    virtualTable.addEntry((((CllistContext)_localctx).fieldName!=null?((CllistContext)_localctx).fieldName.getText():null), fieldEntry);
						                    classType.addFieldType(-fieldEntry.getOffset() - 1, fieldEntry.getType());
						                
						}
						}
						setState(72);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
				}

				setState(75);
				match(RPAR);
				setState(76);
				match(CLPAR);

				                    // Offset dei metodi viene inizializzato a 0 se non si eredita, altrimenti lo setto in base alla
				                    // al numero di metodi della classe da cui estendo, ovvero "lunghezza"
				                    int offsetMethod = 0;
				                    if(superClassEntry.isPresent()) {
				                        ClassType superClassType = ((ClassType) superClassEntry.get().getType());
				                        offsetMethod = superClassType.getMethodsCount();
				                    }
				                
				setState(129);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==FUN) {
					{
					{
					setState(78);
					match(FUN);
					setState(79);
					((CllistContext)_localctx).methodName = match(ID);
					setState(80);
					match(COLON);
					setState(81);
					((CllistContext)_localctx).retType = ((CllistContext)_localctx).type = type();

					                        MethodNode methodNode = new MethodNode((((CllistContext)_localctx).methodName!=null?((CllistContext)_localctx).methodName.getText():null), ((CllistContext)_localctx).retType.mType);
					                        classNode.addMethodNode(methodNode);
					                        STEntry methodEntry = STEntry.makeMethodSTEntry(null, symTable.getCurrentNestingLevel(), offsetMethod++);
					                        // aggiungo a virtual table il metodo
					                        virtualTable.addEntry((((CllistContext)_localctx).methodName!=null?((CllistContext)_localctx).methodName.getText():null), methodEntry);
					                        methodNode.setOffset(methodEntry.getOffset());
					                        // entro nesting level
					                        symTable.enterNestingLevel();
					                    
					setState(83);
					match(LPAR);
					setState(99);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==ID) {
						{
						setState(84);
						((CllistContext)_localctx).paramName = match(ID);
						setState(85);
						match(COLON);
						setState(86);
						((CllistContext)_localctx).parType = hotype();

						                                int offsetParameter = 1;
						                                methodNode.addParam(new ParNode((((CllistContext)_localctx).paramName!=null?((CllistContext)_localctx).paramName.getText():null), ((CllistContext)_localctx).parType.mType));
						                                STEntry paramEntry = new STEntry(((CllistContext)_localctx).parType.mType, symTable.getCurrentNestingLevel(), offsetParameter++);
						                                if (!symTable.addEntry((((CllistContext)_localctx).paramName!=null?((CllistContext)_localctx).paramName.getText():null), paramEntry)){ // controllo che non ci siano parametri con stesso nome
						                                    throw new RuntimeException("parameter: "+(((CllistContext)_localctx).paramName!=null?((CllistContext)_localctx).paramName.getText():null)+ " at line "+(((CllistContext)_localctx).paramName!=null?((CllistContext)_localctx).paramName.getLine():0)+ " already declared.");
						                                }
						                            
						setState(96);
						_errHandler.sync(this);
						_la = _input.LA(1);
						while (_la==COMMA) {
							{
							{
							setState(88);
							match(COMMA);
							setState(89);
							((CllistContext)_localctx).paramName = match(ID);
							setState(90);
							match(COLON);
							setState(91);
							((CllistContext)_localctx).parType = hotype();

							                                        methodNode.addParam(new ParNode((((CllistContext)_localctx).paramName!=null?((CllistContext)_localctx).paramName.getText():null), ((CllistContext)_localctx).parType.mType));
							                                        paramEntry = new STEntry(((CllistContext)_localctx).parType.mType, symTable.getCurrentNestingLevel(), offsetParameter++);
							                                        if (!symTable.addEntry((((CllistContext)_localctx).paramName!=null?((CllistContext)_localctx).paramName.getText():null), paramEntry)){ // controllo che non ci siano parametri con stesso nome
							                                            throw new RuntimeException("parameter: "+(((CllistContext)_localctx).paramName!=null?((CllistContext)_localctx).paramName.getText():null)+ " at line "+(((CllistContext)_localctx).paramName!=null?((CllistContext)_localctx).paramName.getLine():0)+ " already declared.");
							                                        }
							                                    
							}
							}
							setState(98);
							_errHandler.sync(this);
							_la = _input.LA(1);
						}
						}
					}

					setState(101);
					match(RPAR);

					                        // possiamo costruire l'arrow type, che andiamo ad aggiungere a ClassType e alla STEntry che si trova nella virtual table.
					                        methodEntry.addType(methodNode.getSymType());
					                        // per mantenere coerenza con la virtual table in caso di override, il vecchio arrow type del metodo
					                        // viene sostituito dal nuovo arrow type del metodo dichiarato per l'override, infatti viene rimpiazzato
					                        // in posizione offset. Dove l'offset è quello inserito nella STEntry del nuovo metodo.
					                        classType.addMethodType(methodEntry.getOffset(), methodNode.getSymType());
					                    
					setState(121);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==LET) {
						{
						setState(103);
						match(LET);
						  int offsetDec = -2;
						                               List<Node> decList = new ArrayList<>();
						                            
						setState(114); 
						_errHandler.sync(this);
						_la = _input.LA(1);
						do {
							{
							{
							setState(105);
							match(VAR);
							setState(106);
							((CllistContext)_localctx).varName = match(ID);
							setState(107);
							match(COLON);
							setState(108);
							((CllistContext)_localctx).type = type();
							setState(109);
							match(ASS);
							setState(110);
							((CllistContext)_localctx).exp = exp();
							setState(111);
							match(SEMIC);

							                                    decList.add(new VarNode((((CllistContext)_localctx).varName!=null?((CllistContext)_localctx).varName.getText():null), ((CllistContext)_localctx).type.mType, ((CllistContext)_localctx).exp.ast));
							                                    STEntry entry = new STEntry(((CllistContext)_localctx).type.mType, symTable.getCurrentNestingLevel(), offsetDec--);
							                                    if (!symTable.addEntry((((CllistContext)_localctx).varName!=null?((CllistContext)_localctx).varName.getText():null), entry)){
							                                        throw new RuntimeException("variable: "+(((CllistContext)_localctx).varName!=null?((CllistContext)_localctx).varName.getText():null)+ " at line "+(((CllistContext)_localctx).varName!=null?((CllistContext)_localctx).varName.getLine():0)+ " already declared.");
							                                    };
							                                
							}
							}
							setState(116); 
							_errHandler.sync(this);
							_la = _input.LA(1);
						} while ( _la==VAR );
						setState(118);
						match(IN);
						 methodNode.addDeclarationList(decList); 
						}
					}

					setState(123);
					((CllistContext)_localctx).exp = exp();

					                                methodNode.addBody(((CllistContext)_localctx).exp.ast);
					                                symTable.exitNestingLevel();
					                            
					setState(125);
					match(SEMIC);
					}
					}
					setState(131);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(132);
				match(CRPAR);
				 symTable.exitNestingLevel(); /* esco dal livello della dichiarazione della classe.*/ 
				}
				}
				setState(136); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==CLASS );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DeclistContext extends ParserRuleContext {
		public List<Node> decList;
		public Token nomeVar;
		public HotypeContext varType;
		public ExpContext exp;
		public Token nomeFun;
		public TypeContext retType;
		public Token nomePar;
		public HotypeContext parType;
		public DeclistContext declist;
		public List<TerminalNode> SEMIC() { return getTokens(FOOLParser.SEMIC); }
		public TerminalNode SEMIC(int i) {
			return getToken(FOOLParser.SEMIC, i);
		}
		public List<TerminalNode> VAR() { return getTokens(FOOLParser.VAR); }
		public TerminalNode VAR(int i) {
			return getToken(FOOLParser.VAR, i);
		}
		public List<TerminalNode> COLON() { return getTokens(FOOLParser.COLON); }
		public TerminalNode COLON(int i) {
			return getToken(FOOLParser.COLON, i);
		}
		public List<TerminalNode> ASS() { return getTokens(FOOLParser.ASS); }
		public TerminalNode ASS(int i) {
			return getToken(FOOLParser.ASS, i);
		}
		public List<ExpContext> exp() {
			return getRuleContexts(ExpContext.class);
		}
		public ExpContext exp(int i) {
			return getRuleContext(ExpContext.class,i);
		}
		public List<TerminalNode> FUN() { return getTokens(FOOLParser.FUN); }
		public TerminalNode FUN(int i) {
			return getToken(FOOLParser.FUN, i);
		}
		public List<TerminalNode> LPAR() { return getTokens(FOOLParser.LPAR); }
		public TerminalNode LPAR(int i) {
			return getToken(FOOLParser.LPAR, i);
		}
		public List<TerminalNode> RPAR() { return getTokens(FOOLParser.RPAR); }
		public TerminalNode RPAR(int i) {
			return getToken(FOOLParser.RPAR, i);
		}
		public List<TerminalNode> ID() { return getTokens(FOOLParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(FOOLParser.ID, i);
		}
		public List<HotypeContext> hotype() {
			return getRuleContexts(HotypeContext.class);
		}
		public HotypeContext hotype(int i) {
			return getRuleContext(HotypeContext.class,i);
		}
		public List<TypeContext> type() {
			return getRuleContexts(TypeContext.class);
		}
		public TypeContext type(int i) {
			return getRuleContext(TypeContext.class,i);
		}
		public List<TerminalNode> LET() { return getTokens(FOOLParser.LET); }
		public TerminalNode LET(int i) {
			return getToken(FOOLParser.LET, i);
		}
		public List<DeclistContext> declist() {
			return getRuleContexts(DeclistContext.class);
		}
		public DeclistContext declist(int i) {
			return getRuleContext(DeclistContext.class,i);
		}
		public List<TerminalNode> IN() { return getTokens(FOOLParser.IN); }
		public TerminalNode IN(int i) {
			return getToken(FOOLParser.IN, i);
		}
		public List<TerminalNode> COMMA() { return getTokens(FOOLParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(FOOLParser.COMMA, i);
		}
		public DeclistContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_declist; }
	}

	public final DeclistContext declist() throws RecognitionException {
		DeclistContext _localctx = new DeclistContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_declist);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{

			                int offset = (symTable.getCurrentNestingLevel() == 0) ? -classTable.getNumberOfClassDeclaration()-2 : -2;
			                ((DeclistContext)_localctx).decList =  new ArrayList<>();
			            
			setState(186); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(182);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case VAR:
					{
					setState(139);
					match(VAR);
					setState(140);
					((DeclistContext)_localctx).nomeVar = match(ID);
					setState(141);
					match(COLON);
					setState(142);
					((DeclistContext)_localctx).varType = hotype();
					setState(143);
					match(ASS);
					setState(144);
					((DeclistContext)_localctx).exp = exp();

					                //aggiungo il nodo alla lisat delle dichiarazioni (la declist è un insieme di nodi "dichiarazione")
					                _localctx.decList.add(new VarNode((((DeclistContext)_localctx).nomeVar!=null?((DeclistContext)_localctx).nomeVar.getText():null), ((DeclistContext)_localctx).varType.mType, ((DeclistContext)_localctx).exp.ast));
					                STEntry entry= new STEntry(((DeclistContext)_localctx).varType.mType, symTable.getCurrentNestingLevel(),  offset--);
					                if (!symTable.addEntry((((DeclistContext)_localctx).nomeVar!=null?((DeclistContext)_localctx).nomeVar.getText():null), entry)){
					                    throw new RuntimeException("variable: "+(((DeclistContext)_localctx).nomeVar!=null?((DeclistContext)_localctx).nomeVar.getText():null)+ " at line "+(((DeclistContext)_localctx).nomeVar!=null?((DeclistContext)_localctx).nomeVar.getLine():0)+ " already declared.");
					                };
					            
					}
					break;
				case FUN:
					{
					setState(147);
					match(FUN);
					setState(148);
					((DeclistContext)_localctx).nomeFun = match(ID);
					setState(149);
					match(COLON);
					setState(150);
					((DeclistContext)_localctx).retType = type();

					                FunNode fun = new FunNode((((DeclistContext)_localctx).nomeFun!=null?((DeclistContext)_localctx).nomeFun.getText():null), ((DeclistContext)_localctx).retType.mType);
					                _localctx.decList.add(fun);
					                //La costruzione della entry della funzione avviene in diversi passi; prima si passa il nesting level ma ancora prima di leggere i parametri
					                //non possiamo sapere il suo tipo (tipo freccia); per questo passiamo null
					                STEntry funEntry = STEntry.makeFunctionSTEntry(null, symTable.getCurrentNestingLevel(),  offset--);
					                if(!symTable.addEntry((((DeclistContext)_localctx).nomeFun!=null?((DeclistContext)_localctx).nomeFun.getText():null), funEntry)) {
					                    throw new RuntimeException("Function : "+(((DeclistContext)_localctx).nomeFun!=null?((DeclistContext)_localctx).nomeFun.getText():null)+ " at line "+(((DeclistContext)_localctx).nomeFun!=null?((DeclistContext)_localctx).nomeFun.getLine():0)+ " already declared.");
					                }
					                // entro dentro un nuovo scope
					                symTable.enterNestingLevel();
					            
					setState(152);
					match(LPAR);
					setState(168);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==ID) {
						{
						setState(153);
						((DeclistContext)_localctx).nomePar = match(ID);
						setState(154);
						match(COLON);
						setState(155);
						((DeclistContext)_localctx).parType = hotype();

						                int offsetParameter = 1;
						                ParNode parNode = new ParNode((((DeclistContext)_localctx).nomePar!=null?((DeclistContext)_localctx).nomePar.getText():null), ((DeclistContext)_localctx).parType.mType);
						                fun.addParam(parNode);
						                STEntry stEntry = new STEntry(((DeclistContext)_localctx).parType.mType, symTable.getCurrentNestingLevel(), offsetParameter++);
						                if (!symTable.addEntry((((DeclistContext)_localctx).nomePar!=null?((DeclistContext)_localctx).nomePar.getText():null), stEntry)){
						                    throw new RuntimeException("parameter: "+(((DeclistContext)_localctx).nomePar!=null?((DeclistContext)_localctx).nomePar.getText():null)+ " at line "+(((DeclistContext)_localctx).nomePar!=null?((DeclistContext)_localctx).nomePar.getLine():0)+ " already declared.");
						                };
						            
						setState(165);
						_errHandler.sync(this);
						_la = _input.LA(1);
						while (_la==COMMA) {
							{
							{
							setState(157);
							match(COMMA);
							setState(158);
							((DeclistContext)_localctx).nomePar = match(ID);
							setState(159);
							match(COLON);
							setState(160);
							((DeclistContext)_localctx).parType = hotype();

							                parNode = new ParNode((((DeclistContext)_localctx).nomePar!=null?((DeclistContext)_localctx).nomePar.getText():null), ((DeclistContext)_localctx).parType.mType);
							                fun.addParam(parNode);
							                stEntry = new STEntry(((DeclistContext)_localctx).parType.mType, symTable.getCurrentNestingLevel(),  offsetParameter++);
							                if (!symTable.addEntry((((DeclistContext)_localctx).nomePar!=null?((DeclistContext)_localctx).nomePar.getText():null), stEntry)){
							                    throw new RuntimeException("parameter: "+(((DeclistContext)_localctx).nomePar!=null?((DeclistContext)_localctx).nomePar.getText():null)+ " at line "+(((DeclistContext)_localctx).nomePar!=null?((DeclistContext)_localctx).nomePar.getLine():0)+ " already declared.");
							                };

							            
							}
							}
							setState(167);
							_errHandler.sync(this);
							_la = _input.LA(1);
						}
						}
					}

					setState(170);
					match(RPAR);

					                funEntry.addType(fun.getSymType());//fun.getSymType() crea un arrowType in base ai parametry del FunNode e lo restituisce
					                //dopo aver aggiunto le entry dei parametri setto il tipo della funzione nella entry
					            
					setState(177);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==LET) {
						{
						setState(172);
						match(LET);
						setState(173);
						((DeclistContext)_localctx).declist = declist();
						setState(174);
						match(IN);
						fun.addDeclarationList(((DeclistContext)_localctx).declist.decList); 
						}
					}

					setState(179);
					((DeclistContext)_localctx).exp = exp();

					                fun.addBody(((DeclistContext)_localctx).exp.ast);
					                symTable.exitNestingLevel();
					            
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(184);
				match(SEMIC);
				}
				}
				setState(188); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==VAR || _la==FUN );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpContext extends ParserRuleContext {
		public Node ast;
		public TermContext term;
		public TermContext t2;
		public List<TermContext> term() {
			return getRuleContexts(TermContext.class);
		}
		public TermContext term(int i) {
			return getRuleContext(TermContext.class,i);
		}
		public List<TerminalNode> PLUS() { return getTokens(FOOLParser.PLUS); }
		public TerminalNode PLUS(int i) {
			return getToken(FOOLParser.PLUS, i);
		}
		public List<TerminalNode> MINUS() { return getTokens(FOOLParser.MINUS); }
		public TerminalNode MINUS(int i) {
			return getToken(FOOLParser.MINUS, i);
		}
		public List<TerminalNode> OR() { return getTokens(FOOLParser.OR); }
		public TerminalNode OR(int i) {
			return getToken(FOOLParser.OR, i);
		}
		public ExpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_exp; }
	}

	public final ExpContext exp() throws RecognitionException {
		ExpContext _localctx = new ExpContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_exp);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(190);
			((ExpContext)_localctx).term = term();
			 ((ExpContext)_localctx).ast =  ((ExpContext)_localctx).term.ast;
			setState(206);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << PLUS) | (1L << MINUS) | (1L << OR))) != 0)) {
				{
				setState(204);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case PLUS:
					{
					setState(192);
					match(PLUS);
					setState(193);
					((ExpContext)_localctx).t2 = ((ExpContext)_localctx).term = term();
					 ((ExpContext)_localctx).ast =  new PlusNode(_localctx.ast, ((ExpContext)_localctx).t2.ast);
					}
					break;
				case MINUS:
					{
					setState(196);
					match(MINUS);
					setState(197);
					((ExpContext)_localctx).t2 = ((ExpContext)_localctx).term = term();
					 ((ExpContext)_localctx).ast =  new MinusNode(_localctx.ast, ((ExpContext)_localctx).t2.ast);
					}
					break;
				case OR:
					{
					setState(200);
					match(OR);
					setState(201);
					((ExpContext)_localctx).t2 = ((ExpContext)_localctx).term = term();
					 ((ExpContext)_localctx).ast =  new OrNode(_localctx.ast, ((ExpContext)_localctx).t2.ast);
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(208);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TermContext extends ParserRuleContext {
		public Node ast;
		public FactorContext factor;
		public FactorContext f2;
		public List<FactorContext> factor() {
			return getRuleContexts(FactorContext.class);
		}
		public FactorContext factor(int i) {
			return getRuleContext(FactorContext.class,i);
		}
		public List<TerminalNode> TIMES() { return getTokens(FOOLParser.TIMES); }
		public TerminalNode TIMES(int i) {
			return getToken(FOOLParser.TIMES, i);
		}
		public List<TerminalNode> DIV() { return getTokens(FOOLParser.DIV); }
		public TerminalNode DIV(int i) {
			return getToken(FOOLParser.DIV, i);
		}
		public List<TerminalNode> AND() { return getTokens(FOOLParser.AND); }
		public TerminalNode AND(int i) {
			return getToken(FOOLParser.AND, i);
		}
		public TermContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_term; }
	}

	public final TermContext term() throws RecognitionException {
		TermContext _localctx = new TermContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_term);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(209);
			((TermContext)_localctx).factor = factor();
			 ((TermContext)_localctx).ast =  ((TermContext)_localctx).factor.ast;
			setState(225);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << TIMES) | (1L << DIV) | (1L << AND))) != 0)) {
				{
				setState(223);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case TIMES:
					{
					setState(211);
					match(TIMES);
					setState(212);
					((TermContext)_localctx).f2 = ((TermContext)_localctx).factor = factor();
					 ((TermContext)_localctx).ast =  new MultNode(_localctx.ast, ((TermContext)_localctx).f2.ast);
					}
					break;
				case DIV:
					{
					setState(215);
					match(DIV);
					setState(216);
					((TermContext)_localctx).f2 = ((TermContext)_localctx).factor = factor();
					 ((TermContext)_localctx).ast =  new DivNode(_localctx.ast, ((TermContext)_localctx).f2.ast);
					}
					break;
				case AND:
					{
					setState(219);
					match(AND);
					setState(220);
					((TermContext)_localctx).f2 = ((TermContext)_localctx).factor = factor();
					 ((TermContext)_localctx).ast =  new AndNode(_localctx.ast, ((TermContext)_localctx).f2.ast);
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(227);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FactorContext extends ParserRuleContext {
		public Node ast;
		public ValueContext value;
		public ValueContext v2;
		public List<ValueContext> value() {
			return getRuleContexts(ValueContext.class);
		}
		public ValueContext value(int i) {
			return getRuleContext(ValueContext.class,i);
		}
		public List<TerminalNode> EQ() { return getTokens(FOOLParser.EQ); }
		public TerminalNode EQ(int i) {
			return getToken(FOOLParser.EQ, i);
		}
		public List<TerminalNode> GE() { return getTokens(FOOLParser.GE); }
		public TerminalNode GE(int i) {
			return getToken(FOOLParser.GE, i);
		}
		public List<TerminalNode> LE() { return getTokens(FOOLParser.LE); }
		public TerminalNode LE(int i) {
			return getToken(FOOLParser.LE, i);
		}
		public FactorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_factor; }
	}

	public final FactorContext factor() throws RecognitionException {
		FactorContext _localctx = new FactorContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_factor);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(228);
			((FactorContext)_localctx).value = value();
			 ((FactorContext)_localctx).ast =  ((FactorContext)_localctx).value.ast;
			setState(244);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << GE) | (1L << LE) | (1L << EQ))) != 0)) {
				{
				setState(242);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case EQ:
					{
					setState(230);
					match(EQ);
					setState(231);
					((FactorContext)_localctx).v2 = ((FactorContext)_localctx).value = value();
					 ((FactorContext)_localctx).ast =  new EqualNode(_localctx.ast, ((FactorContext)_localctx).v2.ast);
					}
					break;
				case GE:
					{
					setState(234);
					match(GE);
					setState(235);
					((FactorContext)_localctx).v2 = ((FactorContext)_localctx).value = value();
					 ((FactorContext)_localctx).ast =  new GreaterEqualNode(_localctx.ast, ((FactorContext)_localctx).v2.ast);
					}
					break;
				case LE:
					{
					setState(238);
					match(LE);
					setState(239);
					((FactorContext)_localctx).v2 = ((FactorContext)_localctx).value = value();
					 ((FactorContext)_localctx).ast =  new LessEqualNode(_localctx.ast, ((FactorContext)_localctx).v2.ast);
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(246);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ValueContext extends ParserRuleContext {
		public Node ast;
		public Token val;
		public Token id;
		public ExpContext exp;
		public ExpContext cond;
		public ExpContext thenb;
		public ExpContext elseb;
		public Token id2;
		public TerminalNode INTEGER() { return getToken(FOOLParser.INTEGER, 0); }
		public TerminalNode TRUE() { return getToken(FOOLParser.TRUE, 0); }
		public TerminalNode FALSE() { return getToken(FOOLParser.FALSE, 0); }
		public TerminalNode NULL() { return getToken(FOOLParser.NULL, 0); }
		public TerminalNode NEW() { return getToken(FOOLParser.NEW, 0); }
		public TerminalNode LPAR() { return getToken(FOOLParser.LPAR, 0); }
		public TerminalNode RPAR() { return getToken(FOOLParser.RPAR, 0); }
		public List<TerminalNode> ID() { return getTokens(FOOLParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(FOOLParser.ID, i);
		}
		public List<ExpContext> exp() {
			return getRuleContexts(ExpContext.class);
		}
		public ExpContext exp(int i) {
			return getRuleContext(ExpContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(FOOLParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(FOOLParser.COMMA, i);
		}
		public TerminalNode IF() { return getToken(FOOLParser.IF, 0); }
		public TerminalNode THEN() { return getToken(FOOLParser.THEN, 0); }
		public List<TerminalNode> CLPAR() { return getTokens(FOOLParser.CLPAR); }
		public TerminalNode CLPAR(int i) {
			return getToken(FOOLParser.CLPAR, i);
		}
		public List<TerminalNode> CRPAR() { return getTokens(FOOLParser.CRPAR); }
		public TerminalNode CRPAR(int i) {
			return getToken(FOOLParser.CRPAR, i);
		}
		public TerminalNode ELSE() { return getToken(FOOLParser.ELSE, 0); }
		public TerminalNode NOT() { return getToken(FOOLParser.NOT, 0); }
		public TerminalNode PRINT() { return getToken(FOOLParser.PRINT, 0); }
		public TerminalNode DOT() { return getToken(FOOLParser.DOT, 0); }
		public ValueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_value; }
	}

	public final ValueContext value() throws RecognitionException {
		ValueContext _localctx = new ValueContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_value);
		int _la;
		try {
			setState(344);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case INTEGER:
				enterOuterAlt(_localctx, 1);
				{
				setState(247);
				((ValueContext)_localctx).val = match(INTEGER);
				 ((ValueContext)_localctx).ast =  new IntNode(Integer.parseInt((((ValueContext)_localctx).val!=null?((ValueContext)_localctx).val.getText():null))); 
				}
				break;
			case TRUE:
				enterOuterAlt(_localctx, 2);
				{
				setState(249);
				match(TRUE);
				 ((ValueContext)_localctx).ast =  new BoolNode(true); 
				}
				break;
			case FALSE:
				enterOuterAlt(_localctx, 3);
				{
				setState(251);
				match(FALSE);
				 ((ValueContext)_localctx).ast =  new BoolNode(false); 
				}
				break;
			case NULL:
				enterOuterAlt(_localctx, 4);
				{
				setState(253);
				match(NULL);
				 ((ValueContext)_localctx).ast =  new EmptyNode(); 
				}
				break;
			case NEW:
				enterOuterAlt(_localctx, 5);
				{
				setState(255);
				match(NEW);
				setState(256);
				((ValueContext)_localctx).id = match(ID);

					            Optional<STEntry> classEntry = symTable.findClassDeclaration((((ValueContext)_localctx).id!=null?((ValueContext)_localctx).id.getText():null));
					            if(!classTable.existClass((((ValueContext)_localctx).id!=null?((ValueContext)_localctx).id.getText():null)) || !classEntry.isPresent()) {
					                throw new RuntimeException("Instantiation of an undeclared class " + (((ValueContext)_localctx).id!=null?((ValueContext)_localctx).id.getText():null) + " at line " + (((ValueContext)_localctx).id!=null?((ValueContext)_localctx).id.getLine():0));
					            }
					        
				setState(258);
				match(LPAR);
				List<Node> params = new ArrayList<>();
				setState(271);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LPAR) | (1L << NOT) | (1L << TRUE) | (1L << FALSE) | (1L << IF) | (1L << PRINT) | (1L << NEW) | (1L << NULL) | (1L << INTEGER) | (1L << ID))) != 0)) {
					{
					setState(260);
					((ValueContext)_localctx).exp = exp();
					params.add(((ValueContext)_localctx).exp.ast);
					setState(268);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==COMMA) {
						{
						{
						setState(262);
						match(COMMA);
						setState(263);
						((ValueContext)_localctx).exp = exp();
						params.add(((ValueContext)_localctx).exp.ast);
						}
						}
						setState(270);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
				}

				setState(273);
				match(RPAR);
				 ((ValueContext)_localctx).ast =  new NewNode((((ValueContext)_localctx).id!=null?((ValueContext)_localctx).id.getText():null), classEntry.get(), params, symTable.getCurrentNestingLevel()); 
				}
				break;
			case IF:
				enterOuterAlt(_localctx, 6);
				{
				setState(275);
				match(IF);
				setState(276);
				((ValueContext)_localctx).cond = exp();
				setState(277);
				match(THEN);
				setState(278);
				match(CLPAR);
				setState(279);
				((ValueContext)_localctx).thenb = exp();
				setState(280);
				match(CRPAR);
				setState(281);
				match(ELSE);
				setState(282);
				match(CLPAR);
				setState(283);
				((ValueContext)_localctx).elseb = exp();
				setState(284);
				match(CRPAR);
				 ((ValueContext)_localctx).ast =  new IfNode(((ValueContext)_localctx).thenb.ast, ((ValueContext)_localctx).cond.ast, ((ValueContext)_localctx).elseb.ast); 
				}
				break;
			case NOT:
				enterOuterAlt(_localctx, 7);
				{
				setState(287);
				match(NOT);
				setState(288);
				match(LPAR);
				setState(289);
				((ValueContext)_localctx).exp = exp();
				setState(290);
				match(RPAR);
				 ((ValueContext)_localctx).ast =  new NotNode(((ValueContext)_localctx).exp.ast); 
				}
				break;
			case PRINT:
				enterOuterAlt(_localctx, 8);
				{
				setState(293);
				match(PRINT);
				setState(294);
				match(LPAR);
				setState(295);
				((ValueContext)_localctx).exp = exp();
				setState(296);
				match(RPAR);
				 ((ValueContext)_localctx).ast =  new PrintNode(((ValueContext)_localctx).exp.ast); 
				}
				break;
			case LPAR:
				enterOuterAlt(_localctx, 9);
				{
				setState(299);
				match(LPAR);
				setState(300);
				((ValueContext)_localctx).exp = exp();
				setState(301);
				match(RPAR);
				 ((ValueContext)_localctx).ast =  ((ValueContext)_localctx).exp.ast; 
				}
				break;
			case ID:
				enterOuterAlt(_localctx, 10);
				{
				setState(304);
				((ValueContext)_localctx).id = match(ID);

				                Optional<STEntry> idEntry = symTable.findDeclaration((((ValueContext)_localctx).id!=null?((ValueContext)_localctx).id.getText():null));
				                if(idEntry.isPresent()){
				                    ((ValueContext)_localctx).ast =  new IdNode((((ValueContext)_localctx).id!=null?((ValueContext)_localctx).id.getText():null), symTable.getCurrentNestingLevel(), idEntry.get());
				                }else{
				                    throw new RuntimeException("identificatore: "+(((ValueContext)_localctx).id!=null?((ValueContext)_localctx).id.getText():null)+ " at line "+(((ValueContext)_localctx).id!=null?((ValueContext)_localctx).id.getLine():0)+ " never declared.");
				                }
				            
				setState(342);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case LPAR:
					{
					setState(306);
					match(LPAR);
					 List<Node> parameters = new ArrayList();
					setState(319);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LPAR) | (1L << NOT) | (1L << TRUE) | (1L << FALSE) | (1L << IF) | (1L << PRINT) | (1L << NEW) | (1L << NULL) | (1L << INTEGER) | (1L << ID))) != 0)) {
						{
						setState(308);
						((ValueContext)_localctx).exp = exp();
						parameters.add(((ValueContext)_localctx).exp.ast);
						setState(316);
						_errHandler.sync(this);
						_la = _input.LA(1);
						while (_la==COMMA) {
							{
							{
							setState(310);
							match(COMMA);
							setState(311);
							((ValueContext)_localctx).exp = exp();
							parameters.add(((ValueContext)_localctx).exp.ast);
							}
							}
							setState(318);
							_errHandler.sync(this);
							_la = _input.LA(1);
						}
						}
					}

					setState(321);
					match(RPAR);
					 ((ValueContext)_localctx).ast =  new CallNode((((ValueContext)_localctx).id!=null?((ValueContext)_localctx).id.getText():null), idEntry.get(), parameters,  symTable.getCurrentNestingLevel());
					}
					break;
				case DOT:
					{
					setState(323);
					match(DOT);
					setState(324);
					((ValueContext)_localctx).id2 = match(ID);

					                    List<Node> parameters = new ArrayList();
					                    Optional<STEntry> methodEntry = Optional.empty();
					                    if(idEntry.get().getType() instanceof RefType) {
					                        RefType refType = (RefType) idEntry.get().getType();
					                        methodEntry = classTable.getSTEntry(refType.getClassName(), (((ValueContext)_localctx).id2!=null?((ValueContext)_localctx).id2.getText():null));
					                        if(!methodEntry.isPresent()) {
					                            throw new RuntimeException("Method call to a non existing method " + (((ValueContext)_localctx).id2!=null?((ValueContext)_localctx).id2.getText():null) + " at line " + (((ValueContext)_localctx).id2!=null?((ValueContext)_localctx).id2.getLine():0));
					                        }
					                    } else {
					                        throw new RuntimeException("Method call to a non object reference: " + (((ValueContext)_localctx).id!=null?((ValueContext)_localctx).id.getText():null) + " at line " + (((ValueContext)_localctx).id!=null?((ValueContext)_localctx).id.getLine():0));
					                    }
					                
					setState(326);
					match(LPAR);
					setState(338);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LPAR) | (1L << NOT) | (1L << TRUE) | (1L << FALSE) | (1L << IF) | (1L << PRINT) | (1L << NEW) | (1L << NULL) | (1L << INTEGER) | (1L << ID))) != 0)) {
						{
						setState(327);
						((ValueContext)_localctx).exp = exp();
						parameters.add(((ValueContext)_localctx).exp.ast);
						setState(335);
						_errHandler.sync(this);
						_la = _input.LA(1);
						while (_la==COMMA) {
							{
							{
							setState(329);
							match(COMMA);
							setState(330);
							((ValueContext)_localctx).exp = exp();
							parameters.add(((ValueContext)_localctx).exp.ast);
							}
							}
							setState(337);
							_errHandler.sync(this);
							_la = _input.LA(1);
						}
						}
					}

					setState(340);
					match(RPAR);

					                    ((ValueContext)_localctx).ast =  new ClassCallNode((((ValueContext)_localctx).id2!=null?((ValueContext)_localctx).id2.getText():null), idEntry.get(), methodEntry.get(), parameters, symTable.getCurrentNestingLevel());
					                
					}
					break;
				case PLUS:
				case MINUS:
				case TIMES:
				case DIV:
				case RPAR:
				case CRPAR:
				case SEMIC:
				case COMMA:
				case OR:
				case AND:
				case GE:
				case LE:
				case EQ:
				case THEN:
					break;
				default:
					break;
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class HotypeContext extends ParserRuleContext {
		public Type mType;
		public TypeContext type;
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public ArrowContext arrow() {
			return getRuleContext(ArrowContext.class,0);
		}
		public HotypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_hotype; }
	}

	public final HotypeContext hotype() throws RecognitionException {
		HotypeContext _localctx = new HotypeContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_hotype);
		try {
			setState(350);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case INT:
			case BOOL:
			case ID:
				enterOuterAlt(_localctx, 1);
				{
				setState(346);
				((HotypeContext)_localctx).type = type();
				 ((HotypeContext)_localctx).mType =  ((HotypeContext)_localctx).type.mType; 
				}
				break;
			case LPAR:
				enterOuterAlt(_localctx, 2);
				{
				setState(349);
				arrow();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeContext extends ParserRuleContext {
		public Type mType;
		public Token className;
		public TerminalNode INT() { return getToken(FOOLParser.INT, 0); }
		public TerminalNode BOOL() { return getToken(FOOLParser.BOOL, 0); }
		public TerminalNode ID() { return getToken(FOOLParser.ID, 0); }
		public TypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type; }
	}

	public final TypeContext type() throws RecognitionException {
		TypeContext _localctx = new TypeContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_type);
		try {
			setState(358);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case INT:
				enterOuterAlt(_localctx, 1);
				{
				setState(352);
				match(INT);
				 ((TypeContext)_localctx).mType =  IntType.TYPE; 
				}
				break;
			case BOOL:
				enterOuterAlt(_localctx, 2);
				{
				setState(354);
				match(BOOL);
				 ((TypeContext)_localctx).mType =  BoolType.TYPE; 
				}
				break;
			case ID:
				enterOuterAlt(_localctx, 3);
				{
				setState(356);
				((TypeContext)_localctx).className = match(ID);

				            if(classTable.existClass((((TypeContext)_localctx).className!=null?((TypeContext)_localctx).className.getText():null))) {
				                ((TypeContext)_localctx).mType =  RefType.of((((TypeContext)_localctx).className!=null?((TypeContext)_localctx).className.getText():null));
				            } else {
				                throw new RuntimeException("Type " + (((TypeContext)_localctx).className!=null?((TypeContext)_localctx).className.getText():null) + " at line " + (((TypeContext)_localctx).className!=null?((TypeContext)_localctx).className.getLine():0) + " unknown");
				            }
				 	    
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArrowContext extends ParserRuleContext {
		public TerminalNode LPAR() { return getToken(FOOLParser.LPAR, 0); }
		public TerminalNode RPAR() { return getToken(FOOLParser.RPAR, 0); }
		public TerminalNode ARROW() { return getToken(FOOLParser.ARROW, 0); }
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public List<HotypeContext> hotype() {
			return getRuleContexts(HotypeContext.class);
		}
		public HotypeContext hotype(int i) {
			return getRuleContext(HotypeContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(FOOLParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(FOOLParser.COMMA, i);
		}
		public ArrowContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_arrow; }
	}

	public final ArrowContext arrow() throws RecognitionException {
		ArrowContext _localctx = new ArrowContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_arrow);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(360);
			match(LPAR);
			setState(369);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LPAR) | (1L << INT) | (1L << BOOL) | (1L << ID))) != 0)) {
				{
				setState(361);
				hotype();
				setState(366);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(362);
					match(COMMA);
					setState(363);
					hotype();
					}
					}
					setState(368);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(371);
			match(RPAR);
			setState(372);
			match(ARROW);
			setState(373);
			type();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3+\u017a\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\5\2\37\n\2\3\2\3\2\3\2\5\2$\n\2\3"+
		"\2\3\2\3\2\3\2\3\2\3\2\3\2\5\2-\n\2\3\2\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3"+
		"\3\3\3\5\39\n\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\7\3G\n"+
		"\3\f\3\16\3J\13\3\5\3L\n\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3"+
		"\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\7\3a\n\3\f\3\16\3d\13\3\5\3f\n\3\3\3"+
		"\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\6\3u\n\3\r\3\16\3v\3"+
		"\3\3\3\3\3\5\3|\n\3\3\3\3\3\3\3\3\3\7\3\u0082\n\3\f\3\16\3\u0085\13\3"+
		"\3\3\3\3\6\3\u0089\n\3\r\3\16\3\u008a\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4"+
		"\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\7"+
		"\4\u00a6\n\4\f\4\16\4\u00a9\13\4\5\4\u00ab\n\4\3\4\3\4\3\4\3\4\3\4\3\4"+
		"\3\4\5\4\u00b4\n\4\3\4\3\4\3\4\5\4\u00b9\n\4\3\4\3\4\6\4\u00bd\n\4\r\4"+
		"\16\4\u00be\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\7"+
		"\5\u00cf\n\5\f\5\16\5\u00d2\13\5\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3"+
		"\6\3\6\3\6\3\6\3\6\7\6\u00e2\n\6\f\6\16\6\u00e5\13\6\3\7\3\7\3\7\3\7\3"+
		"\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\7\7\u00f5\n\7\f\7\16\7\u00f8\13"+
		"\7\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b"+
		"\3\b\3\b\7\b\u010d\n\b\f\b\16\b\u0110\13\b\5\b\u0112\n\b\3\b\3\b\3\b\3"+
		"\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b"+
		"\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3"+
		"\b\3\b\3\b\7\b\u013d\n\b\f\b\16\b\u0140\13\b\5\b\u0142\n\b\3\b\3\b\3\b"+
		"\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\7\b\u0150\n\b\f\b\16\b\u0153\13\b"+
		"\5\b\u0155\n\b\3\b\3\b\5\b\u0159\n\b\5\b\u015b\n\b\3\t\3\t\3\t\3\t\5\t"+
		"\u0161\n\t\3\n\3\n\3\n\3\n\3\n\3\n\5\n\u0169\n\n\3\13\3\13\3\13\3\13\7"+
		"\13\u016f\n\13\f\13\16\13\u0172\13\13\5\13\u0174\n\13\3\13\3\13\3\13\3"+
		"\13\3\13\2\2\f\2\4\6\b\n\f\16\20\22\24\2\2\2\u019f\2\26\3\2\2\2\4\61\3"+
		"\2\2\2\6\u008c\3\2\2\2\b\u00c0\3\2\2\2\n\u00d3\3\2\2\2\f\u00e6\3\2\2\2"+
		"\16\u015a\3\2\2\2\20\u0160\3\2\2\2\22\u0168\3\2\2\2\24\u016a\3\2\2\2\26"+
		",\b\2\1\2\27\30\7\34\2\2\30#\b\2\1\2\31\32\5\4\3\2\32\36\b\2\1\2\33\34"+
		"\5\6\4\2\34\35\b\2\1\2\35\37\3\2\2\2\36\33\3\2\2\2\36\37\3\2\2\2\37$\3"+
		"\2\2\2 !\5\6\4\2!\"\b\2\1\2\"$\3\2\2\2#\31\3\2\2\2# \3\2\2\2$%\3\2\2\2"+
		"%&\7\35\2\2&\'\5\b\5\2\'(\b\2\1\2(-\3\2\2\2)*\5\b\5\2*+\b\2\1\2+-\3\2"+
		"\2\2,\27\3\2\2\2,)\3\2\2\2-.\3\2\2\2./\7\13\2\2/\60\b\2\1\2\60\3\3\2\2"+
		"\2\61\u0088\b\3\1\2\62\63\7 \2\2\63\64\7(\2\2\648\b\3\1\2\65\66\7!\2\2"+
		"\66\67\7(\2\2\679\b\3\1\28\65\3\2\2\289\3\2\2\29:\3\2\2\2:;\b\3\1\2;K"+
		"\7\7\2\2<=\7(\2\2=>\7\f\2\2>?\5\22\n\2?H\b\3\1\2@A\7\r\2\2AB\7(\2\2BC"+
		"\7\f\2\2CD\5\22\n\2DE\b\3\1\2EG\3\2\2\2F@\3\2\2\2GJ\3\2\2\2HF\3\2\2\2"+
		"HI\3\2\2\2IL\3\2\2\2JH\3\2\2\2K<\3\2\2\2KL\3\2\2\2LM\3\2\2\2MN\7\b\2\2"+
		"NO\7\t\2\2O\u0083\b\3\1\2PQ\7\37\2\2QR\7(\2\2RS\7\f\2\2ST\5\22\n\2TU\b"+
		"\3\1\2Ue\7\7\2\2VW\7(\2\2WX\7\f\2\2XY\5\20\t\2Yb\b\3\1\2Z[\7\r\2\2[\\"+
		"\7(\2\2\\]\7\f\2\2]^\5\20\t\2^_\b\3\1\2_a\3\2\2\2`Z\3\2\2\2ad\3\2\2\2"+
		"b`\3\2\2\2bc\3\2\2\2cf\3\2\2\2db\3\2\2\2eV\3\2\2\2ef\3\2\2\2fg\3\2\2\2"+
		"gh\7\b\2\2h{\b\3\1\2ij\7\34\2\2jt\b\3\1\2kl\7\36\2\2lm\7(\2\2mn\7\f\2"+
		"\2no\5\22\n\2op\7\25\2\2pq\5\b\5\2qr\7\13\2\2rs\b\3\1\2su\3\2\2\2tk\3"+
		"\2\2\2uv\3\2\2\2vt\3\2\2\2vw\3\2\2\2wx\3\2\2\2xy\7\35\2\2yz\b\3\1\2z|"+
		"\3\2\2\2{i\3\2\2\2{|\3\2\2\2|}\3\2\2\2}~\5\b\5\2~\177\b\3\1\2\177\u0080"+
		"\7\13\2\2\u0080\u0082\3\2\2\2\u0081P\3\2\2\2\u0082\u0085\3\2\2\2\u0083"+
		"\u0081\3\2\2\2\u0083\u0084\3\2\2\2\u0084\u0086\3\2\2\2\u0085\u0083\3\2"+
		"\2\2\u0086\u0087\7\n\2\2\u0087\u0089\b\3\1\2\u0088\62\3\2\2\2\u0089\u008a"+
		"\3\2\2\2\u008a\u0088\3\2\2\2\u008a\u008b\3\2\2\2\u008b\5\3\2\2\2\u008c"+
		"\u00bc\b\4\1\2\u008d\u008e\7\36\2\2\u008e\u008f\7(\2\2\u008f\u0090\7\f"+
		"\2\2\u0090\u0091\5\20\t\2\u0091\u0092\7\25\2\2\u0092\u0093\5\b\5\2\u0093"+
		"\u0094\b\4\1\2\u0094\u00b9\3\2\2\2\u0095\u0096\7\37\2\2\u0096\u0097\7"+
		"(\2\2\u0097\u0098\7\f\2\2\u0098\u0099\5\22\n\2\u0099\u009a\b\4\1\2\u009a"+
		"\u00aa\7\7\2\2\u009b\u009c\7(\2\2\u009c\u009d\7\f\2\2\u009d\u009e\5\20"+
		"\t\2\u009e\u00a7\b\4\1\2\u009f\u00a0\7\r\2\2\u00a0\u00a1\7(\2\2\u00a1"+
		"\u00a2\7\f\2\2\u00a2\u00a3\5\20\t\2\u00a3\u00a4\b\4\1\2\u00a4\u00a6\3"+
		"\2\2\2\u00a5\u009f\3\2\2\2\u00a6\u00a9\3\2\2\2\u00a7\u00a5\3\2\2\2\u00a7"+
		"\u00a8\3\2\2\2\u00a8\u00ab\3\2\2\2\u00a9\u00a7\3\2\2\2\u00aa\u009b\3\2"+
		"\2\2\u00aa\u00ab\3\2\2\2\u00ab\u00ac\3\2\2\2\u00ac\u00ad\7\b\2\2\u00ad"+
		"\u00b3\b\4\1\2\u00ae\u00af\7\34\2\2\u00af\u00b0\5\6\4\2\u00b0\u00b1\7"+
		"\35\2\2\u00b1\u00b2\b\4\1\2\u00b2\u00b4\3\2\2\2\u00b3\u00ae\3\2\2\2\u00b3"+
		"\u00b4\3\2\2\2\u00b4\u00b5\3\2\2\2\u00b5\u00b6\5\b\5\2\u00b6\u00b7\b\4"+
		"\1\2\u00b7\u00b9\3\2\2\2\u00b8\u008d\3\2\2\2\u00b8\u0095\3\2\2\2\u00b9"+
		"\u00ba\3\2\2\2\u00ba\u00bb\7\13\2\2\u00bb\u00bd\3\2\2\2\u00bc\u00b8\3"+
		"\2\2\2\u00bd\u00be\3\2\2\2\u00be\u00bc\3\2\2\2\u00be\u00bf\3\2\2\2\u00bf"+
		"\7\3\2\2\2\u00c0\u00c1\5\n\6\2\u00c1\u00d0\b\5\1\2\u00c2\u00c3\7\3\2\2"+
		"\u00c3\u00c4\5\n\6\2\u00c4\u00c5\b\5\1\2\u00c5\u00cf\3\2\2\2\u00c6\u00c7"+
		"\7\4\2\2\u00c7\u00c8\5\n\6\2\u00c8\u00c9\b\5\1\2\u00c9\u00cf\3\2\2\2\u00ca"+
		"\u00cb\7\17\2\2\u00cb\u00cc\5\n\6\2\u00cc\u00cd\b\5\1\2\u00cd\u00cf\3"+
		"\2\2\2\u00ce\u00c2\3\2\2\2\u00ce\u00c6\3\2\2\2\u00ce\u00ca\3\2\2\2\u00cf"+
		"\u00d2\3\2\2\2\u00d0\u00ce\3\2\2\2\u00d0\u00d1\3\2\2\2\u00d1\t\3\2\2\2"+
		"\u00d2\u00d0\3\2\2\2\u00d3\u00d4\5\f\7\2\u00d4\u00e3\b\6\1\2\u00d5\u00d6"+
		"\7\5\2\2\u00d6\u00d7\5\f\7\2\u00d7\u00d8\b\6\1\2\u00d8\u00e2\3\2\2\2\u00d9"+
		"\u00da\7\6\2\2\u00da\u00db\5\f\7\2\u00db\u00dc\b\6\1\2\u00dc\u00e2\3\2"+
		"\2\2\u00dd\u00de\7\20\2\2\u00de\u00df\5\f\7\2\u00df\u00e0\b\6\1\2\u00e0"+
		"\u00e2\3\2\2\2\u00e1\u00d5\3\2\2\2\u00e1\u00d9\3\2\2\2\u00e1\u00dd\3\2"+
		"\2\2\u00e2\u00e5\3\2\2\2\u00e3\u00e1\3\2\2\2\u00e3\u00e4\3\2\2\2\u00e4"+
		"\13\3\2\2\2\u00e5\u00e3\3\2\2\2\u00e6\u00e7\5\16\b\2\u00e7\u00f6\b\7\1"+
		"\2\u00e8\u00e9\7\24\2\2\u00e9\u00ea\5\16\b\2\u00ea\u00eb\b\7\1\2\u00eb"+
		"\u00f5\3\2\2\2\u00ec\u00ed\7\22\2\2\u00ed\u00ee\5\16\b\2\u00ee\u00ef\b"+
		"\7\1\2\u00ef\u00f5\3\2\2\2\u00f0\u00f1\7\23\2\2\u00f1\u00f2\5\16\b\2\u00f2"+
		"\u00f3\b\7\1\2\u00f3\u00f5\3\2\2\2\u00f4\u00e8\3\2\2\2\u00f4\u00ec\3\2"+
		"\2\2\u00f4\u00f0\3\2\2\2\u00f5\u00f8\3\2\2\2\u00f6\u00f4\3\2\2\2\u00f6"+
		"\u00f7\3\2\2\2\u00f7\r\3\2\2\2\u00f8\u00f6\3\2\2\2\u00f9\u00fa\7\'\2\2"+
		"\u00fa\u015b\b\b\1\2\u00fb\u00fc\7\26\2\2\u00fc\u015b\b\b\1\2\u00fd\u00fe"+
		"\7\27\2\2\u00fe\u015b\b\b\1\2\u00ff\u0100\7#\2\2\u0100\u015b\b\b\1\2\u0101"+
		"\u0102\7\"\2\2\u0102\u0103\7(\2\2\u0103\u0104\b\b\1\2\u0104\u0105\7\7"+
		"\2\2\u0105\u0111\b\b\1\2\u0106\u0107\5\b\5\2\u0107\u010e\b\b\1\2\u0108"+
		"\u0109\7\r\2\2\u0109\u010a\5\b\5\2\u010a\u010b\b\b\1\2\u010b\u010d\3\2"+
		"\2\2\u010c\u0108\3\2\2\2\u010d\u0110\3\2\2\2\u010e\u010c\3\2\2\2\u010e"+
		"\u010f\3\2\2\2\u010f\u0112\3\2\2\2\u0110\u010e\3\2\2\2\u0111\u0106\3\2"+
		"\2\2\u0111\u0112\3\2\2\2\u0112\u0113\3\2\2\2\u0113\u0114\7\b\2\2\u0114"+
		"\u015b\b\b\1\2\u0115\u0116\7\30\2\2\u0116\u0117\5\b\5\2\u0117\u0118\7"+
		"\31\2\2\u0118\u0119\7\t\2\2\u0119\u011a\5\b\5\2\u011a\u011b\7\n\2\2\u011b"+
		"\u011c\7\32\2\2\u011c\u011d\7\t\2\2\u011d\u011e\5\b\5\2\u011e\u011f\7"+
		"\n\2\2\u011f\u0120\b\b\1\2\u0120\u015b\3\2\2\2\u0121\u0122\7\21\2\2\u0122"+
		"\u0123\7\7\2\2\u0123\u0124\5\b\5\2\u0124\u0125\7\b\2\2\u0125\u0126\b\b"+
		"\1\2\u0126\u015b\3\2\2\2\u0127\u0128\7\33\2\2\u0128\u0129\7\7\2\2\u0129"+
		"\u012a\5\b\5\2\u012a\u012b\7\b\2\2\u012b\u012c\b\b\1\2\u012c\u015b\3\2"+
		"\2\2\u012d\u012e\7\7\2\2\u012e\u012f\5\b\5\2\u012f\u0130\7\b\2\2\u0130"+
		"\u0131\b\b\1\2\u0131\u015b\3\2\2\2\u0132\u0133\7(\2\2\u0133\u0158\b\b"+
		"\1\2\u0134\u0135\7\7\2\2\u0135\u0141\b\b\1\2\u0136\u0137\5\b\5\2\u0137"+
		"\u013e\b\b\1\2\u0138\u0139\7\r\2\2\u0139\u013a\5\b\5\2\u013a\u013b\b\b"+
		"\1\2\u013b\u013d\3\2\2\2\u013c\u0138\3\2\2\2\u013d\u0140\3\2\2\2\u013e"+
		"\u013c\3\2\2\2\u013e\u013f\3\2\2\2\u013f\u0142\3\2\2\2\u0140\u013e\3\2"+
		"\2\2\u0141\u0136\3\2\2\2\u0141\u0142\3\2\2\2\u0142\u0143\3\2\2\2\u0143"+
		"\u0144\7\b\2\2\u0144\u0159\b\b\1\2\u0145\u0146\7\16\2\2\u0146\u0147\7"+
		"(\2\2\u0147\u0148\b\b\1\2\u0148\u0154\7\7\2\2\u0149\u014a\5\b\5\2\u014a"+
		"\u0151\b\b\1\2\u014b\u014c\7\r\2\2\u014c\u014d\5\b\5\2\u014d\u014e\b\b"+
		"\1\2\u014e\u0150\3\2\2\2\u014f\u014b\3\2\2\2\u0150\u0153\3\2\2\2\u0151"+
		"\u014f\3\2\2\2\u0151\u0152\3\2\2\2\u0152\u0155\3\2\2\2\u0153\u0151\3\2"+
		"\2\2\u0154\u0149\3\2\2\2\u0154\u0155\3\2\2\2\u0155\u0156\3\2\2\2\u0156"+
		"\u0157\7\b\2\2\u0157\u0159\b\b\1\2\u0158\u0134\3\2\2\2\u0158\u0145\3\2"+
		"\2\2\u0158\u0159\3\2\2\2\u0159\u015b\3\2\2\2\u015a\u00f9\3\2\2\2\u015a"+
		"\u00fb\3\2\2\2\u015a\u00fd\3\2\2\2\u015a\u00ff\3\2\2\2\u015a\u0101\3\2"+
		"\2\2\u015a\u0115\3\2\2\2\u015a\u0121\3\2\2\2\u015a\u0127\3\2\2\2\u015a"+
		"\u012d\3\2\2\2\u015a\u0132\3\2\2\2\u015b\17\3\2\2\2\u015c\u015d\5\22\n"+
		"\2\u015d\u015e\b\t\1\2\u015e\u0161\3\2\2\2\u015f\u0161\5\24\13\2\u0160"+
		"\u015c\3\2\2\2\u0160\u015f\3\2\2\2\u0161\21\3\2\2\2\u0162\u0163\7$\2\2"+
		"\u0163\u0169\b\n\1\2\u0164\u0165\7%\2\2\u0165\u0169\b\n\1\2\u0166\u0167"+
		"\7(\2\2\u0167\u0169\b\n\1\2\u0168\u0162\3\2\2\2\u0168\u0164\3\2\2\2\u0168"+
		"\u0166\3\2\2\2\u0169\23\3\2\2\2\u016a\u0173\7\7\2\2\u016b\u0170\5\20\t"+
		"\2\u016c\u016d\7\r\2\2\u016d\u016f\5\20\t\2\u016e\u016c\3\2\2\2\u016f"+
		"\u0172\3\2\2\2\u0170\u016e\3\2\2\2\u0170\u0171\3\2\2\2\u0171\u0174\3\2"+
		"\2\2\u0172\u0170\3\2\2\2\u0173\u016b\3\2\2\2\u0173\u0174\3\2\2\2\u0174"+
		"\u0175\3\2\2\2\u0175\u0176\7\b\2\2\u0176\u0177\7&\2\2\u0177\u0178\5\22"+
		"\n\2\u0178\25\3\2\2\2%\36#,8HKbev{\u0083\u008a\u00a7\u00aa\u00b3\u00b8"+
		"\u00be\u00ce\u00d0\u00e1\u00e3\u00f4\u00f6\u010e\u0111\u013e\u0141\u0151"+
		"\u0154\u0158\u015a\u0160\u0168\u0170\u0173";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}